FROM python:3.10.14-bookworm
MAINTAINER Jamie Wolf

# work around TZ data prompting for interaction
ENV TZ=America/Toronto
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y \
    build-essential \
    ca-certificates \
    gcc \
    make \
    cifs-utils \
    git

RUN apt-get install -y \
    libpq5 \
    libpq-dev \
    python3-dev \
    libssl-dev \
    openssl \
    libxml2-dev \
    libxslt1-dev \
    tzdata

# Create a folder to host the main project files
RUN mkdir -p /opt/roguestudio/roguebot
RUN mkdir -p /opt/roguebotStore
RUN mkdir -p /opt/roguebotLog
WORKDIR /opt/roguestudio/roguebot

# Copy the project files to the image
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# Install libserdes
COPY libserdes-1.0.2.tar.gz libserdes-1.0.2.tar.gz
RUN pip3 install libserdes-1.0.2.tar.gz


# Copy bot to docker
COPY libroguebot libroguebot
COPY libroguebot/model model
COPY docker docker
COPY migrations migrations
COPY roguebot.py ./

RUN chmod +x docker/startup.sh

CMD ["/opt/roguestudio/roguebot/docker/startup.sh"]
