"""empty message

Revision ID: 88fea35964cf
Revises: e8b37667fb18
Create Date: 2023-03-21 20:39:48.484850

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '88fea35964cf'
down_revision = 'e8b37667fb18'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('maprenderrequest',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('requestJson', sa.JSON(), nullable=True),
    sa.Column('userId', sa.BigInteger(), nullable=True),
    sa.Column('channelId', sa.BigInteger(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_maprenderrequest_id'), 'maprenderrequest', ['id'], unique=False)
    op.create_table('userratelimit',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('userId', sa.BigInteger(), nullable=True),
    sa.Column('lastMapRenderRequest', sa.BigInteger(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_userratelimit_id'), 'userratelimit', ['id'], unique=False)
    op.create_index(op.f('ix_userratelimit_userId'), 'userratelimit', ['userId'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_userratelimit_userId'), table_name='userratelimit')
    op.drop_index(op.f('ix_userratelimit_id'), table_name='userratelimit')
    op.drop_table('userratelimit')
    op.drop_index(op.f('ix_maprenderrequest_id'), table_name='maprenderrequest')
    op.drop_table('maprenderrequest')
    # ### end Alembic commands ###
