"""empty message

Revision ID: dd6731d15bb6
Revises: bcb5758b915d
Create Date: 2023-01-25 16:49:19.235646

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'dd6731d15bb6'
down_revision = 'bcb5758b915d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('roleplaycharacter', sa.Column('messageId', sa.BigInteger(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('roleplaycharacter', 'messageId')
    # ### end Alembic commands ###
