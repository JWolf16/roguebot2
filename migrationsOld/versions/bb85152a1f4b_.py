"""empty message

Revision ID: bb85152a1f4b
Revises: dd6731d15bb6
Create Date: 2023-01-25 17:00:15.675245

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bb85152a1f4b'
down_revision = 'dd6731d15bb6'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('roleplaycharacter', sa.Column('channelId', sa.BigInteger(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('roleplaycharacter', 'channelId')
    # ### end Alembic commands ###
