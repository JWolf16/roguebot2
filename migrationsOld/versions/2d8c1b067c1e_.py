"""empty message

Revision ID: 2d8c1b067c1e
Revises: b6ae452e69c4
Create Date: 2024-03-15 23:35:44.323694

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2d8c1b067c1e'
down_revision = 'b6ae452e69c4'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('piratenetrequest',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('channelId', sa.BigInteger(), nullable=True),
    sa.Column('messageId', sa.BigInteger(), nullable=True),
    sa.Column('isActive', sa.Boolean(), nullable=True),
    sa.Column('createdAt', sa.DateTime(), nullable=True),
    sa.Column('expiresAt', sa.BigInteger(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_piratenetrequest_id'), 'piratenetrequest', ['id'], unique=False)
    op.create_index(op.f('ix_piratenetrequest_messageId'), 'piratenetrequest', ['messageId'], unique=False)
    op.create_table('piratenetchoices',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('requestId', sa.Integer(), nullable=True),
    sa.Column('emojiId', sa.String(length=128), nullable=True),
    sa.Column('systemName', sa.String(length=255), nullable=True),
    sa.Column('systemId', sa.Integer(), nullable=True),
    sa.Column('systemPosition', sa.String(length=64), nullable=True),
    sa.Column('votes', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['requestId'], ['piratenetrequest.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_piratenetchoices_emojiId'), 'piratenetchoices', ['emojiId'], unique=False)
    op.create_index(op.f('ix_piratenetchoices_id'), 'piratenetchoices', ['id'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_piratenetchoices_id'), table_name='piratenetchoices')
    op.drop_index(op.f('ix_piratenetchoices_emojiId'), table_name='piratenetchoices')
    op.drop_table('piratenetchoices')
    op.drop_index(op.f('ix_piratenetrequest_messageId'), table_name='piratenetrequest')
    op.drop_index(op.f('ix_piratenetrequest_id'), table_name='piratenetrequest')
    op.drop_table('piratenetrequest')
    # ### end Alembic commands ###
