"""empty message

Revision ID: 7ecde46f500a
Revises: f981c0b0b83a
Create Date: 2021-12-08 23:18:54.724999

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7ecde46f500a'
down_revision = 'f981c0b0b83a'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('reminders',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('userId', sa.BigInteger(), nullable=True),
    sa.Column('mentions', sa.String(length=255, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('remindTime', sa.BigInteger(), nullable=True),
    sa.Column('channelId', sa.BigInteger(), nullable=True),
    sa.Column('guildId', sa.BigInteger(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_reminders_id'), 'reminders', ['id'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_reminders_id'), table_name='reminders')
    op.drop_table('reminders')
    # ### end Alembic commands ###
