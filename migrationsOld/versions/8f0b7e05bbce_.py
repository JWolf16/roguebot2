"""empty message

Revision ID: 8f0b7e05bbce
Revises: 21c7fd9dc426
Create Date: 2021-09-22 00:48:00.285963

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8f0b7e05bbce'
down_revision = '21c7fd9dc426'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('vetrequest', sa.Column('currentState', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('vetrequest', 'currentState')
    # ### end Alembic commands ###
