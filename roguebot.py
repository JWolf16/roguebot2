import asyncio
import string
import sys

from libroguebot import BotSettings, EResponseType, DynamicResponse, SavedData, ENGINE, User, HistoryEntry, \
    EBanFlags, WarRoles, ServerSettings, ServerSetup, ArchiveRecord, ETimeType, Reminders, DiscordTicket, DbDumpDef, \
    ETimeParseResult, UserManagementModule, Command, CommandAuth
import os
import discord
from discord.ext import tasks
import requests
import traceback
import datetime
import time
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import OperationalError
import logging
import logging.handlers
import os.path
import json
import pprint
import random
import uuid
import platform


random.seed()

# Silence urllib3's output for self signed cert, this is dumb that a library has an output logger by default
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

INT_MAX_LOG_SIZE = 1024 * 1024 * 5  # ~5MB size
LOG_DIR = '/opt/roguebotLog/'
LOG_FILE = LOG_DIR + 'roguebot_log.txt'

if not os.path.isdir(LOG_DIR):
    os.makedirs(LOG_DIR)

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)
obj_handler = logging.handlers.RotatingFileHandler(LOG_FILE, maxBytes=INT_MAX_LOG_SIZE, backupCount=5)
obj_handler.setLevel(logging.INFO)
obj_formatter = logging.Formatter('%(name)-18s %(levelname)-8s %(funcName)-25s %(asctime)-25s %(message)s')
obj_handler.setFormatter(obj_formatter)
LOGGER.addHandler(obj_handler)

def getIdList(stIds):
    ret = []
    if stIds is None:
        return ret
    ltIds = stIds.split(";")
    for sId in ltIds:
        try:
            iId = int(sId)
            ret.append(iId)
        except:
            pass
    return ret

TOKEN = os.getenv("DISCORD_BOT_SECRET")
RW_TOKEN = os.getenv("RW_BOT_SECRET")
BOTSETTINGSFILE = '/opt/roguebotStore/botsettings.json'
WARROLESFILE = '/opt/roguebotStore/warroles.json'
SAVEDDATAFILE = '/opt/roguebotStore/savedata.json'
SERVERSETTINGSFILE = '/opt/roguebotStore/serversettings.json'
SERVERSETTINGSTMP = '/opt/roguebotStore/serversettingstmp.json'
FILESTORE = '/opt/roguebotStore/files/'
ARCHIVESTORE = '/opt/roguebotStore/archives/'


OWNERS = getIdList(os.getenv("DISCORD_OWNING_USER_ID"))
RESTRICTED_ROLES = getIdList(os.getenv("DISCORD_RESTRICTED_ROLES"))

ADMIN_CMD_ROLES = getIdList(os.getenv("DISCORD_ADMIN_CMD_ROLES"))
SUPPORT_BOT_IDS = getIdList(os.getenv("DISCORD_SUPPORT_BOT_ID"))
FACTION_REP_ROLES = getIdList(os.getenv("DISCORD_FACTION_REP_ROLES"))
ROLE_MASTER_ROLES = getIdList(os.getenv("DISCORD_ROLE_MASTER_ROLES"))
TEST_MODE = os.getenv("TEST_MODE", default="No")


if not os.path.exists(FILESTORE):
    os.makedirs(FILESTORE)


class Roguebot(discord.Client):

    MaxDice = 1600
    MaxSides = 100000000

    CmdPrefix = "!"
    if TEST_MODE.lower() == "yes":
        CmdPrefix = "r!"

    listAutoBannedUserCmd = CmdPrefix + 'sbreport'



    def __init__(self, logger):
        intents = discord.Intents.default()
        intents.members = True
        intents.presences = True
        intents.message_content = True
        discord.Client.__init__(self, intents=intents, max_messages=7500)
        self.logger = logger
        self.settings = BotSettings()
        self.settings.setPath(BOTSETTINGSFILE)
        if os.path.isfile(BOTSETTINGSFILE):
            self.settings.fromFile(BOTSETTINGSFILE)
            self.settings.rebuildCommandMap()
        self.serverSettings = ServerSettings()
        if os.path.isfile(SERVERSETTINGSFILE):
            self.serverSettings.fromFile(SERVERSETTINGSFILE)
            self.serverSettings.updateCaches()
        self.SavedData = SavedData()
        if os.path.exists(SAVEDDATAFILE):
            self.SavedData.fromFile(SAVEDDATAFILE)
        self.OwnerLevelCmd = DynamicResponse()
        self.OwnerLevelCmd.restrict = True
        self.AdminOnlyCmd = DynamicResponse()
        self.AdminOnlyCmd.restrict = True
        self.AdminOnlyCmd.restrictedRoles = ADMIN_CMD_ROLES
        self.RestrictedCmd = DynamicResponse()
        self.RestrictedCmd.restrict = True
        self.RestrictedCmd.restrictedRoles = RESTRICTED_ROLES
        self.RaffleCmd = DynamicResponse()
        self.RaffleCmd.restrict = True
        self.DynaRestrictCmd = DynamicResponse()
        self.DynaRestrictCmd.restrict = True
        self.userAgent = 'RogueBot'
        self.SessionMaker = sessionmaker()
        self.SessionMaker.configure(bind=ENGINE)
        self.Session = self.SessionMaker()

        self.CommandMap = {}
        self.initComplete = False

        self.DynaModAuth = CommandAuth(OWNERS)
        self.DynaModAuth.restrict = True
        self.DynaModAuth.restrictMember = 'moderatorCmdRoles'


        self.userManagementModule = None # type: UserManagementModule

    async def setup_hook(self) -> None:
        self.checkExpiredReminderRequests.start()

    async def initModulesAndCommands(self):

        self.userManagementModule = UserManagementModule(self.logger, self.settings, self.serverSettings, self.Session, self, FILESTORE)

        await self.userManagementModule.initOnlyBans()

        self.CommandMap = {
            self.listAutoBannedUserCmd: Command(self.userManagementModule.listSubscribers, "List SpamBots that were autobanned", restriction=self.DynaModAuth)
        }

        self.logger.info("Modules and commands initialized!")

    async def delayedCommand(self, delay, command):
        await asyncio.sleep(delay)
        await command()

    def startSqlEntry(self):
        """
        Make the SQL connection work, MySQL drops connections after long periods of inactivity, in theory SQLAlchemy's
        settings should handle this, but for whatever reason they dont seem to, so screw it and do it this way


        :return:
        :rtype:
        """
        try:
            self.Session.commit()
        except:
            self.Session.rollback()
            self.Session.commit()

    async def listGuilds(self, message:discord.Message):
        retStr = '```\nGuild MemberShips & Config status:\n\n'
        for server in self.guilds:
            hasConfig = self.serverSettings.getServer(server.id) is not None
            retStr += f'{server.name} ({server.id}) - {hasConfig}\n'
        retStr += '```'
        await message.channel.send(retStr)

    async def leaveGuild(self, message:discord.Message):
        serverName = message.content.split(' ')[1]
        for server in self.guilds:
            if server.name == serverName:
                await message.channel.send('Matched Server, attempting to leave...')
                await server.leave()
                await message.channel.send(f'Left server: {server.name}')


    def getTicketPerms(self, server:discord.Guild, user, serverSettings:ServerSetup, modsOnly=False):
        channelPerms = discord.PermissionOverwrite(create_instant_invite=False, manage_channels=False,
                                                   add_reactions=True, read_messages=True, send_messages=True,
                                                   send_tts_messages=False, manage_messages=False, embed_links=True,
                                                   attach_files=True, read_message_history=True, mention_everyone=False,
                                                   external_emojis=True, connect=True, speak=True, mute_members=False,
                                                   deafen_members=False, move_members=False, use_voice_activation=True,
                                                   manage_roles=False, manage_webhooks=False)
        modPerms = discord.PermissionOverwrite(create_instant_invite=False, manage_channels=True,
                                               add_reactions=True, read_messages=True, send_messages=True,
                                               send_tts_messages=False, manage_messages=False, embed_links=True,
                                               attach_files=True, read_message_history=True, mention_everyone=False,
                                               external_emojis=True, connect=True, speak=True, mute_members=False,
                                               deafen_members=False, move_members=False, use_voice_activation=True,
                                               manage_roles=False, manage_webhooks=False)
        defaultPerms = discord.PermissionOverwrite(create_instant_invite=False, manage_channels=False,
                                                   add_reactions=False, read_messages=False, send_messages=False,
                                                   send_tts_messages=False, manage_messages=False, embed_links=False,
                                                   attach_files=False, read_message_history=False, mention_everyone=False,
                                                   external_emojis=False, connect=False, speak=False, mute_members=False,
                                                   deafen_members=False, move_members=False, use_voice_activation=False,
                                                   manage_roles=False, manage_webhooks=False)
        permOverites = {
            server.default_role: defaultPerms
        }
        for role in server.roles:
            if role.id in serverSettings.ticketMods:
                permOverites[role] = modPerms
            elif role.id in serverSettings.ticketRoles and not modsOnly:
                permOverites[role] = channelPerms
        if user is not None:
            permOverites[user] = channelPerms
        return permOverites

    async def timeoutUser(self, member: discord.Member, timeSpan, reason):
        await member.timeout(timeSpan, reason=reason)

    async def addUserFlag(self, message, flag, type, cmd):
        try:
            self.startSqlEntry()
            flaggedUser = None
            mentionStr = ""
            dUser = None # type: discord.Member
            stContent = message.content.replace(cmd, "", 1)
            serverSettings = self.serverSettings.getServer(message.guild.id)
            for user in message.mentions: # type: discord.Member
                dbUser = self.Session.query(User).filter(User.userId == user.id, User.ServerGroup == serverSettings.reporterGroup).first()
                if dbUser is not None:
                    flaggedUser = dbUser
                    mentionStr = user.mention
                    dUser = user
                    break
            if flaggedUser is None:
                flaggedUser = User()
                try:
                    mention = message.mentions[0] # type: discord.Member
                    flaggedUser.userId = mention.id
                    flaggedUser.mentions = mention.mention
                    flaggedUser.userName = mention.name
                    mentionStr = mention.mention
                    flaggedUser.ServerGroup = serverSettings.reporterGroup
                    dUser = user
                except:
                    print("Failed to get user by mention!")
                    try:
                        iId = int(stContent.split()[0])
                        dbUser = self.Session.query(User).filter(User.id == iId).first() # type: User
                        if dbUser is not None:
                            flaggedUser = dbUser
                            mentionStr = stContent.split()[0]
                            dUser = await message.guild.fetch_member(dbUser.userId)
                        else:
                            return "no such user!"
                    except:
                        trace = traceback.format_exc()
                        lst_trace = trace.split('\n')
                        for line in lst_trace:
                            LOGGER.critical(line)
                        return "unable to process user"
            self.logger.info(f'content: {stContent}, mStr: {mentionStr}')
            stContent = stContent.replace(mentionStr, "")
            parseResult, iAmount, stMod, stText = self.parseTimeSpan(stContent.split())
            if parseResult == ETimeParseResult.Ok:
                stContent = stText
            if flag != EBanFlags.Null:
                if flag not in [EBanFlags.MuteDropped, EBanFlags.SMuteDropped, EBanFlags.Appealed, EBanFlags.ModNote]:
                    flaggedUser.setFlag(flag, True)
                elif flag == EBanFlags.MuteDropped:
                    flaggedUser.setFlag(EBanFlags.Mute, False)
                elif flag == EBanFlags.SMuteDropped:
                    flaggedUser.setFlag(EBanFlags.SMute, False)
                event = HistoryEntry()
                event.user_id = flaggedUser.id
                event.setFlag(flag, True)
                event.comments = message.content.replace(mentionStr, "").replace(cmd, "", 1)
                event.timeStamp = time.time()
                event.reporter = message.author.name
                flaggedUser.history.append(event)
            self.Session.add(flaggedUser)
            if flag != EBanFlags.Null:
                self.Session.add(event)
            self.Session.commit()
            if flag == EBanFlags.Mute:
                if dUser is not None:
                    if ETimeType.timeOk(iAmount, stMod, ETimeType.MaxDiscordTimeOut):
                        timeSpan = ETimeType.getTimeDelta(iAmount, stMod)
                        if timeSpan:
                            await self.timeoutUser(dUser, timeSpan, stContent)
                        else:
                            return f"Could not timeout user - No Timespan"
                    else:
                        return f"Could not timeout user - Invalid Timespan"
                else:
                    return f"Failed to find user to timeout"
            if flag == EBanFlags.MuteDropped:
                if dUser is not None:
                    await self.timeoutUser(dUser, None, stContent)
                else:
                    return f"Failed to find user to unmute"
            if flag == EBanFlags.SMute:
                if dUser is not None:
                    muteRole = message.guild.get_role(serverSettings.supportMuteRole)
                    if muteRole is not None:
                        await dUser.add_roles(muteRole, reason=stContent.replace(mentionStr, ""))
                    else:
                        return f"Could not S-Mute! {serverSettings.supportMuteRole}"
                else:
                    return f"Failed to find user to s-mute"
            if flag == EBanFlags.SMuteDropped:
                if dUser is not None:
                    muteRole = message.guild.get_role(serverSettings.supportMuteRole)
                    if muteRole is not None:
                        await dUser.remove_roles(muteRole, reason=stContent.replace(mentionStr, ""))
                    else:
                        return f"Could not Unsmute!"
                else:
                    return f"Failed to find user to unsmute"
            return f"User has been {type}"
        except OperationalError:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                LOGGER.critical(line)
            self.Session.rollback()
            self.Session.commit()
        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                LOGGER.critical(line)
        return "Error! report to Jamie and try again"


    def removeUser(self, id, serverGroup):
        try:
            self.startSqlEntry()
            dbUser = self.Session.query(User).filter(User.id == id, User.ServerGroup == serverGroup).first() # type: User
            if dbUser is not None:
                for event in dbUser.history:
                    self.Session.delete(event)
                self.Session.delete(dbUser)
                self.Session.commit()
            else:
                return "No such user!"
            return f"User removed from records"
        except OperationalError:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                LOGGER.critical(line)
            self.Session.rollback()
            self.Session.commit()
        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                LOGGER.critical(line)
        return "Error! report to Jamie and try again"

    def listUsers(self, serverGroup):
        ret = ["**Reported Users:** \n"]
        lRet = []
        length = 0
        try:
            self.startSqlEntry()
            dbUsers = self.Session.query(User).filter(User.ServerGroup == serverGroup).all() # type: list[User]
            for user in dbUsers:
                stUser = f"ID: {user.id}, DiscordID: {user.userId}, Name: {user.userName}\n"
                length += len(stUser)
                if length >= 1750:
                    lRet.append("".join(ret))
                    ret = []
                    length = 0
                ret.append(stUser)
            ret.append("\nTo Enquire further use !userrecord command")
            lRet.append("".join(ret))
            return lRet
        except OperationalError:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                LOGGER.critical(line)
            self.Session.rollback()
            self.Session.commit()
        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                LOGGER.critical(line)
        return "Error! report to Jamie and try again"

    def getUserStatus(self, user):
        ret = []
        if user.isBanned:
            ret.append("Banned")
        if user.isSMuted:
            ret.append("Support-Muted")
        if user.isMuted:
            ret.append("Muted")
        if user.isWarned:
            ret.append("Warned")
        if user.isMutedDropped:
            ret.append("Mute Dropped")
        if user.isSMutedDropped:
            ret.append("S-Mute Dropped")
        if user.isAppeal:
            ret.append("Appeal")
        if user.isNote:
            ret.append("Note")
        if len(ret) == 0:
            return "All Good"
        return ", ".join(ret)


    def getUserRecord(self, id, bdiscordId, serverGroup):
        ret = []
        try:
            self.startSqlEntry()
            if bdiscordId:
                user = self.Session.query(User).filter(User.userId == id, User.ServerGroup == serverGroup).first() # type: User
            else:
                user = self.Session.query(User).filter(User.id == id, User.ServerGroup == serverGroup).first() # type: User
            if user is not None:
                ret.append(f"**Known Data on User: {id}**")
                stUser = f"DiscordID: {user.userId}, Name: {user.userName}"
                ret.append(stUser)
                ret.append(f"Status: {self.getUserStatus(user)}")
                ret.append(f"Notes:\n{user.notes}")
                ret.append("User Record:")
                for event in user.history: # type: HistoryEntry
                    ret.append(f"{datetime.datetime.utcfromtimestamp(event.timeStamp).isoformat()} - Type: {self.getUserStatus(event)}, Reporter: {event.reporter}, - Notes: {event.comments}")
            else:
                return "No user record found!"
            return "\n".join(ret)
        except OperationalError:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                LOGGER.critical(line)
            self.Session.rollback()
            self.Session.commit()
        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                LOGGER.critical(line)
        return "Error! report to Jamie and try again"


    async def canRunRestrictedCommand(self, message, command):
        author = message.author # type: discord.Member
        try:
            if command.canRun(author, OWNERS):
                return True
            else:
                await message.channel.send("Command access restricted")
        except:
            await message.channel.send("Command not available")
        return False

    async def sendFile(self, filename, channel):

        resource = os.path.join(FILESTORE, filename)
        if os.path.exists(resource):
            dFile = discord.File(resource)
            await channel.send(file=dFile)
        else:
            await channel.send("Resource not found!")

    async def shutdownBot(self, message: discord.Message):
        await message.channel.send('https://tenor.com/view/old-yeller-gif-6180444')
        await self.close()
        sys.exit(0)

    async def dumpDb(self, message: discord.Message):
        await message.channel.send(embed=self.makeInformationalEmbed("Creating DB Dump..."))
        try:
            self.startSqlEntry()
            dumpDef = DbDumpDef()
            dbUsers = self.Session.query(User).all()  # type: list[User]
            for record in dbUsers:
                dumpDef.modHistory.append(record.toDef())
            dbArchives = self.Session.query(ArchiveRecord).all()  # type: list[ArchiveRecord]
            for record in dbArchives:
                dumpDef.archives.append(record.toDef())
            dbReminders = self.Session.query(Reminders).all()  # type: list[Reminders]
            for record in dbReminders:
                dumpDef.reminders.append(record.toDef())
            dbTickets = self.Session.query(DiscordTicket).all()  # type: list[DiscordTicket]
            for record in dbTickets:
                dumpDef.tickets.append(record.toDef())
            dbDumpFile = 'dbDump.json'
            resource = os.path.join(FILESTORE, dbDumpFile)
            dumpDef.toFile(resource)
            await message.channel.send(embed=self.makeSuccessEmbed("Dump created"))
            await self.sendFile(dbDumpFile, message.channel)
        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                LOGGER.critical(line)
            await message.channel.send(embed=self.makeErrorEmbed("Dump Failed!"))

    async def importDb(self, message: discord.Message):
        if len(message.attachments) == 0:
            await message.channel.send(embed=self.makeErrorEmbed("Could not find data to import!"))
            return
        dumpDef = DbDumpDef()
        bData = await message.attachments[0].read()
        dumpDef.fromJson(bData)

        await message.channel.send(embed=self.makeInformationalEmbed("Importing DB Dump..."))
        idx = 0
        self.startSqlEntry()
        for recordDef in dumpDef.archives:
            record = ArchiveRecord()
            record.fromDef(recordDef)
            self.Session.add(record)
            idx += 1
            if idx % 25 == 0:
                self.Session.commit()
        self.Session.commit()
        for recordDef in dumpDef.reminders:
            record = Reminders()
            record.fromDef(recordDef)
            self.Session.add(record)
            idx += 1
            if idx % 25 == 0:
                self.Session.commit()
        self.Session.commit()
        for recordDef in dumpDef.tickets:
            record = DiscordTicket()
            record.fromDef(recordDef)
            self.Session.add(record)
            idx += 1
            if idx % 25 == 0:
                self.Session.commit()
        self.Session.commit()
        histRecords = 0
        for recordDef in dumpDef.modHistory:
            record = User()
            record.fromDef(recordDef)
            histRecords += len(record.history)
            self.Session.add(record)
            idx += 1
            if idx % 25 == 0:
                self.Session.commit()
        self.Session.commit()
        await message.channel.send(embed=self.makeSuccessEmbed(f"Import complete, created: {idx + histRecords}"))


    # ToDo move to Mrbc
    async def sendInnMessage(self, message:discord.Message, stMsg):
        url = f'https://161.97.74.91:16001/api/rogueadmin/bkmessage'
        headers = {'User-Agent': 'Roguebot', 'content-type': 'application/json', 'X-RB-TOKEN': RW_TOKEN}
        jData = {
                'Call' : 'SendBotMsg',
                'Msg' : stMsg,
                'CId' : 738881197249265680
            }
        r = requests.post(url, headers=headers, data=json.dumps(jData).encode(), verify=False)
        await message.channel.send('message queued')

    async def freeze_channel(self, channel:discord.TextChannel):
        eonePerms = channel.overwrites_for(channel.guild.default_role)
        if eonePerms.read_messages is not None and not eonePerms.read_messages:
            await channel.send("channel is private, will not freeze")
            await channel.send("https://tenor.com/view/i-dont-want-to-touch-that-chef-south-park-s1e6-death-gif-21526130")
            return
        serverSettings = self.serverSettings.getServer(channel.guild.id)
        if len(serverSettings.moderatorCmdRoles) == 0:
            return

        genPerms = discord.Permissions.none()
        genPerms.read_messages=True
        await channel.set_permissions(channel.guild.default_role, send_messages=False)
        for role in serverSettings.moderatorCmdRoles:
            await channel.set_permissions(channel.guild.get_role(role), send_messages=True)
        await channel.send("channel is frozen!")
        await channel.send("https://tenor.com/view/youre-frozen-claire-crosby-the-crosbys-claire-and-the-crosbys-freeze-gif-17606178")

    async def runDynamicCommand(self, message:discord.Message, dynaCommand:DynamicResponse):
        if not dynaCommand.canRun(message.author, OWNERS):
            return
        try:
            await message.channel.typing()
        except:
            pass
        for role in dynaCommand.pingRoles:
            await message.channel.send(role)
        if dynaCommand.ResponseType == EResponseType.String:
            if dynaCommand.asEmbed:
                embed = discord.Embed(colour=dynaCommand.embedColour)
                embed.description = dynaCommand.Response.replace('\n', '\n\n')
                await message.channel.send(embed=embed)
            else:
                await message.channel.send(dynaCommand.Response)
        elif dynaCommand.ResponseType == EResponseType.File:
            await self.sendFile(dynaCommand.Response, message.channel)
        elif dynaCommand.ResponseType == EResponseType.RandomChoose:
            response = dynaCommand.getResponse()
            try:
                iIndex = int(message.content.split()[-1])
                if iIndex < len(dynaCommand.possibleResponses):
                    response = dynaCommand.possibleResponses[iIndex]
            except:
                pass

            if response.ResponseType == EResponseType.String:
                if dynaCommand.asEmbed:
                    embed = discord.Embed(colour=dynaCommand.embedColour)
                    embed.description = response.Response.replace('\n', '\n\n')
                    await message.channel.send(embed=embed)
                else:
                    await message.channel.send(response.Response)
            elif response.ResponseType == EResponseType.File:
                await self.sendFile(response.Response, message.channel)
        elif dynaCommand.ResponseType == EResponseType.ChannelReHome:
            if 'ticket' in message.channel.name.lower() or 'closed' in message.channel.name.lower():
                server = message.channel.guild  # type: discord.Guild
                settings = self.serverSettings.getServer(server.id)
                categories = server.categories
                targetCategory = None
                for cat in categories:
                    if cat.name.lower() == dynaCommand.Response:
                        targetCategory = cat
                        break
                if not targetCategory:
                    await message.channel.send(embed=self.makeErrorEmbed("Failed to locate channel group!"))
                await self.closeOrMoveTicket(message, targetCategory, settings, False)
                # await message.channel.edit(category=targetCategory)
                # await message.channel.edit(sync_permissions=True)
                # perms = self.getTicketPerms(server, None, settings)
                # for role in perms:
                #     await message.channel.set_permissions(role, overwrite=perms[role])

    async def closeOrMoveTicket(self, message:discord.Message, targetCategory, serverSettings:ServerSetup, close):
        if targetCategory is None:
            # no place to move tickets!
            await message.channel.send(
                f"Could not move the ticket! target category: {serverSettings.closedTicketCat} not found")
            return
        channelName = message.channel.name
        if close:
            channelName += '-closed'
        await message.channel.edit(name=channelName, category=targetCategory)
        await message.channel.edit(sync_permissions=True)
        perms = self.getTicketPerms(message.channel.guild, None, serverSettings)
        for role in perms:
            await message.channel.set_permissions(role, overwrite=perms[role])
        if serverSettings.dmOnTicketClose:
            self.startSqlEntry()
            ticket = self.Session.query(DiscordTicket).filter(DiscordTicket.channelId == message.channel.id).first() # type: DiscordTicket
            if ticket:
                if not ticket.alerted:
                    embed = discord.Embed(colour=discord.Colour.blurple(), title=serverSettings.dmTicketMsgPrefix.format(ticket.ticketId, message.guild.name))
                    stMsg = serverSettings.dmTicketMsg
                    if len(message.content.split()) > 1:
                        stMsg = ' '.join(message.content.split()[1:])
                    embed.description = stMsg
                    user = await self.fetch_user(ticket.userId)
                    if user:
                        await user.send(embed=embed)
                        ticket.alerted = True
                        self.Session.add(ticket)
                        self.Session.commit()


    async def resolveUsers(self, message:discord.Message, bUseMentions):

        dIds = []
        if bUseMentions:
            for user in message.mentions:
                dIds.append(user.id)
        else:
            ids = message.content.split()[1:]
            for id in ids:
                dIds.append(id)

        settings = self.serverSettings.getServer(message.guild.id)
        for dId in dIds:
            user = await self.fetch_user(dId)
            try:
                member = await message.guild.fetch_member(dId)
            except:
                member = None
            embed = discord.Embed(colour=discord.Colour.dark_blue(), title=f"Resolved User: {user.display_name}")
            if member:
                embed.set_author(name=member.display_name, icon_url=member.display_avatar.url)
                if member.joined_at is not None:
                    embed.add_field(name=f'Joined {settings.serverName} On', value=f'{member.joined_at.strftime(settings.welcomeLogStrFmt)}',
                                    inline=False)
                embed.add_field(name='Created On', value=f'{member.created_at.strftime(settings.welcomeLogStrFmt)}', inline=False)
                embed.add_field(name="Discord ID", value=member.id, inline=False)
                embed.add_field(name="User Name", value=member.name, inline=False)
                embed.add_field(name="Spammer?", value=str(member.public_flags.spammer), inline=False)
                embed.add_field(name="Highest Role", value=member.top_role.name, inline=False)
                embed.add_field(name="flags", value=','.join( str(x.name) for x in member.public_flags.all()), inline=False)
            else:
                embed.set_author(name=user.display_name, icon_url=user.display_avatar.url)
                embed.add_field(name='Created On', value=f'{user.created_at.strftime(settings.welcomeLogStrFmt)}',
                                inline=False)
                embed.add_field(name="Discord ID", value=user.id, inline=False)
                embed.add_field(name="User Name", value=user.name, inline=False)
                embed.add_field(name="Spammer?", value=str(user.public_flags.spammer), inline=False)
                embed.add_field(name="flags", value=','.join( str(x.name) for x in user.public_flags.all()), inline=False)
            embed.set_footer(text="RogueBot", icon_url=self.user.display_avatar.url)
            await message.channel.send(embed=embed)
            time.sleep(1.5)

    async def archiveChannel(self, message:discord.Message):
        serverSettings = self.serverSettings.getServer(message.guild.id)
        messageCount = 1000
        count = 0
        try:
            messageCount = int(message.content.split()[-1])
        except:
            pass
        fName = f'{uuid.uuid4()}.txt'
        if not os.path.exists(ARCHIVESTORE):
            os.makedirs(ARCHIVESTORE)
        with open(f'{ARCHIVESTORE}{fName}', 'wb') as f:
            f.write(f'Recording of Channel: {message.channel.name} - {datetime.datetime.fromtimestamp(time.time()).strftime(serverSettings.welcomeLogStrFmt)}\n\n'.encode())
            async for message in message.channel.history(limit=messageCount, oldest_first=True):
                f.write(f'{message.author.name} ({message.author.id}) - {message.created_at.strftime(serverSettings.welcomeLogStrFmt)}\n'.encode())
                contentlines = []
                line = ''
                content = message.content
                for mention in message.mentions:
                    content = content.replace(mention.mention, f'@{mention.name}')
                for word in content.split():
                    if len(line) >= 120:
                        contentlines.append(line)
                        line = ''
                    line += word + ' '
                if line != '':
                    contentlines.append(line)
                for line in contentlines:
                    f.write(f'\t{line}\n'.encode())
                for attachment in message.attachments:
                    f.write(f'{attachment.url}\n'.encode())
                f.write('\n'.encode())
                count += 1
            self.startSqlEntry()
            record = ArchiveRecord()
            record.channelId = message.channel.id
            record.channelName = message.channel.name
            record.guildId = message.guild.id
            record.fileName = fName
            record.timestamp = time.time()
            self.Session.add(record)
            self.Session.commit()
        embed = discord.Embed(colour=discord.Colour.dark_purple())
        embed.description = f'Archived {count} messages'
        await message.channel.send(embed=embed)

    async def listArchives(self, message:discord.Message):
        serverSettings = self.serverSettings.getServer(message.guild.id)
        term = DynamicResponse().getExtras(message.content)
        self.startSqlEntry()
        if term != "":
            Archives = self.Session.query(ArchiveRecord).filter(ArchiveRecord.guildId == message.guild.id, ArchiveRecord.channelName.contains(term)).all() # type: list[ArchiveRecord]
        else:
            Archives = self.Session.query(ArchiveRecord).filter(ArchiveRecord.guildId == message.guild.id).all() # type: list[ArchiveRecord]
        embed = discord.Embed(colour=discord.Colour.dark_teal())
        # embed.set_thumbnail(url=self.user.display_avatar.url)
        embed.description = f'Archives for: {serverSettings.serverName}'
        ltMsg = []
        for archive in Archives:
            ltMsg.append(f'{archive.id} - {archive.channelName} - {datetime.datetime.utcfromtimestamp(archive.timestamp).strftime(serverSettings.archiveStrFmt)}')
            if len(ltMsg) >= 24:
                embed.add_field(name='Available Archives', value='\n'.join(ltMsg))
                await message.channel.send(embed=embed)
                embed = discord.Embed(colour=discord.Colour.dark_teal())
                embed.set_image(url=self.user.display_avatar.url)
                embed.description = f'Archives for: {serverSettings.serverName}'
                ltMsg = []
                time.sleep(0.1)
        embed.add_field(name='Available Archives', value='\n'.join(ltMsg))
        await message.channel.send(embed=embed)

    def getArchiveId(self, message:discord.Message):
        ret = -1
        try:
            ret = int(message.content.split()[1])
        except:
            pass
        return ret

    async def removeArchive(self, message:discord.Message):
        arcId = self.getArchiveId(message)
        if arcId == -1:
            embed = discord.Embed(colour=discord.Colour.brand_red())
            embed.description = f'Failed to parse archive ID'
            await message.channel.send(embed=embed)
            return
        try:
            self.startSqlEntry()
            self.Session.query(ArchiveRecord).filter(ArchiveRecord.id == arcId).delete()
            self.Session.commit()
            embed = discord.Embed(colour=discord.Colour.brand_green())
            embed.description = f'removed Archive'
            await message.channel.send(embed=embed)
            return
        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                self.logger.critical(line)
        embed = discord.Embed(colour=discord.Colour.brand_red())
        embed.description = f'No such Archive ID Found'
        await message.channel.send(embed=embed)

    async def getArchive(self, message:discord.Message):
        arcId = self.getArchiveId(message)
        if arcId == -1:
            embed = discord.Embed(colour=discord.Colour.brand_red())
            embed.description = f'Failed to parse archive ID'
            await message.channel.send(embed=embed)
            return
        self.startSqlEntry()
        archive = self.Session.query(ArchiveRecord).filter(ArchiveRecord.id == arcId).first() # type: ArchiveRecord
        if archive:
            dFile = discord.File(os.path.join(ARCHIVESTORE, archive.fileName))
            await message.channel.send(file=dFile)
        else:
            embed = discord.Embed(colour=discord.Colour.brand_red())
            embed.description = f'No such Archive ID Found'
            await message.channel.send(embed=embed)

    def makeErrorEmbed(self, msg):
        embed = discord.Embed(colour=discord.Colour.brand_red())
        embed.description = msg
        embed.set_footer(text="RogueBot", icon_url=self.user.display_avatar.url)
        return embed

    def makeSuccessEmbed(self, msg):
        embed = discord.Embed(colour=discord.Colour.brand_green())
        embed.description = msg
        embed.set_footer(text="RogueBot", icon_url=self.user.display_avatar.url)
        return embed

    def makeInformationalEmbed(self, msg):
        embed = discord.Embed(colour=discord.Colour.dark_purple())
        embed.description = msg
        embed.set_footer(text="RogueBot", icon_url=self.user.display_avatar.url)
        return embed

    def parseTimeSpan(self, stSpan):
        stAmount = ''
        stMod = ''
        stText = ''
        if len(stSpan) >= 1:
            idx = 0
            for char in stSpan[0]:
                idx += 1
                if char in string.digits:
                    stAmount += char
                else:
                    break
            stMod = stSpan[0][idx - 1:]
            self.logger.info(f'parsed: {stAmount}, {stMod}')
        if len(stSpan) >= 2:
            stText = ' '.join(stSpan[1:])

        iAmount = 0
        try:
            iAmount = int(stAmount)
        except:
            return ETimeParseResult.InvalidTime, None, None, stSpan
        stMod = ETimeType.getDeltaMod(stMod)
        if not stMod:
            return ETimeParseResult.InvalidUnit, None, None, stSpan
        return ETimeParseResult.Ok, iAmount, stMod, stText

    async def createReminder(self, message:discord.Message):
        mContent = message.content.split()[1:]
        parseResult, iAmount, stMod, stText = self.parseTimeSpan(mContent)

        if parseResult != ETimeParseResult.Ok:
            if parseResult == ETimeParseResult.InvalidTime:
                await message.channel.send(embed=self.makeErrorEmbed('Invalid Integer'))
            elif parseResult == ETimeParseResult.InvalidUnit:
                await message.channel.send(embed=self.makeErrorEmbed('Invalid Time Format'))
            else:
                await message.channel.send(embed=self.makeErrorEmbed('Unknown Time parse error'))
            return
        if not ETimeType.timeOk(iAmount, stMod, ETimeType.MaxTimeMap):
            await message.channel.send(embed=self.makeErrorEmbed('Max Time Exceeded'))
            return
        reminder = Reminders()
        reminder.userId = message.author.id
        reminder.mentions = message.author.mention
        reminder.channelId = message.channel.id
        reminder.guildId = message.guild.id
        reminder.setTime(iAmount, stMod)
        reminder.reminderText = stText
        stTime = reminder.reminderTime
        self.startSqlEntry()
        self.Session.add(reminder)
        self.Session.commit()
        await message.channel.send(embed=self.makeSuccessEmbed(f'Reminder Set For: {stTime}'))

    async def splitUpMsg(self, stMsg, channel):
        if len(stMsg) >= 1800:
            idx = 1500
            while idx < len(stMsg):
                if stMsg[idx] == ' ':
                    break
                idx += 1
            await channel.send(stMsg[:idx])
            time.sleep(0.1)
            await self.splitUpMsg(stMsg[idx:], channel)
        else:
            await channel.send(stMsg)

    def rolldie(self, sides, toRoll):
        diceResults = []
        sided = min(sides, self.MaxSides)
        rolls = min(toRoll, self.MaxDice)
        for x in range(rolls):
            diceResults.append(random.randint(1, sided))
        return f'You rolled {rolls}, {sided}-sided die, results:\n{str(diceResults)} Sum: {sum(diceResults)}'

    async def rolldice(self, message:discord.Message):
        try:
            ltMsg = message.content.split()
            ltMsg.pop(0)
            if len(ltMsg) == 0:
                msg = self.rolldie(6, 1)
                await message.channel.send(msg)
            while len(ltMsg) > 0:
                diceToRoll = 1
                sides = 6
                data = ltMsg.pop(0).split('d')
                diceToRoll = int(data[0])
                if len(data) >= 2:
                    sides = int(data[1])
                stMsg = self.rolldie(sides, diceToRoll)
                await self.splitUpMsg(stMsg, message.channel)
        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                self.logger.critical(line)
            await message.channel.send(
                'Failed to roll die, cmd format: **!roll XdY** where X is the number of die to roll, and Y is the number of sides the die have')

    @tasks.loop(seconds=120)
    async def checkExpiredReminderRequests(self):
        try:
            cTime = time.time()
            self.startSqlEntry()
            expiredRequests = self.Session.query(Reminders).filter(Reminders.remindTime <= cTime).all()  # type: list[Reminders]
            for reminder in expiredRequests:
                self.logger.info(f'poking user: {reminder.userId}, in {reminder.channelId}')
                serverSettings = self.serverSettings.getServer(reminder.guildId)
                channel = self.get_channel(reminder.channelId)
                if channel:
                    if reminder.text == '':
                        await channel.send(f'{reminder.mentions} {serverSettings.reminderMsg}')
                    else:
                        await channel.send(f'{reminder.mentions} ```{reminder.text}```')
                    if serverSettings.reminderMeme is not None:
                        await channel.send(serverSettings.reminderMeme)
                self.Session.delete(reminder)
            self.Session.commit()
        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                self.logger.critical(line)

    @checkExpiredReminderRequests.before_loop
    async def beforeReminderHandle(self):
        await self.wait_until_ready()

    async def on_ready(self):
        print(f'{self.user} has connected to Discord, using PY: {platform.python_version()}, DS: {discord.__version__}')
        if not self.initComplete:
            await self.initModulesAndCommands()

    async def on_guild_channel_create(self, channel:discord.abc.GuildChannel):
        if channel.name.startswith("ticket-"):
            server = channel.guild # type: discord.Guild
            settings = self.serverSettings.getServer(server.id)
            categories = server.categories
            searchingFor = "support"
            supportChan = "support"
            catCount = 1
            while True:
                for cat in categories:
                    if cat.name.lower() == searchingFor:
                        if len(cat.channels) >= 50:
                            catCount += 1
                            searchingFor = supportChan + str(catCount)
                            break
                        else:
                            # ticket overflow, but we have space for it, so move it
                            await channel.edit(category=cat)
                            return
                else:
                    stMsg = ""
                    for role in server.roles:
                        if role.id in settings.pingOnTicketsFull:
                            stMsg += f"{role.mention}, "
                    stMsg += f'Plx Halp, the ticket list is full please create: support{catCount} for me. Thanks <3'
                    try:
                        await channel.typing()
                    except:
                        pass
                    await channel.send(stMsg)
                    await channel.send("https://tenor.com/view/shitter-shitters-ful-snow-unclogging-gif-17419118")
                    return

    async def on_raw_message_delete(self, payload: discord.RawMessageDeleteEvent):
        try:
            settings = self.serverSettings.getServer(payload.guild_id)
            if settings.enableMsgDeleteRecords:
                if payload.channel_id in settings.messageDeleteExempt:
                    return
                channel = self.get_channel(settings.messageDeleteChannel)
                affectedChannel = self.get_channel(payload.channel_id)
                if affectedChannel.category.id in settings.messageDeleteCategoryExempt:
                    return
                if channel and affectedChannel:
                    msg = f'Message: {payload.message_id}, deleted from {affectedChannel.name} ({affectedChannel.mention})'
                    embed = self.makeInformationalEmbed(msg)
                    bBot = False
                    files = []
                    if payload.cached_message is not None:
                        message = payload.cached_message
                        if not payload.cached_message.author.bot:
                            embed.set_author(name=f'{message.author.display_name}', icon_url=message.author.display_avatar.url)
                            embed.add_field(name='Author', value=f'{message.author.display_name} ({message.author.name})', inline=False)
                            embed.add_field(name='Author ID', value=f'{message.author.id}')
                            if message.content is not None and message.content != '':
                                embed.add_field(name='Content', value=message.content, inline=False)
                            iAttach = 1
                            for attachment in message.attachments:
                                # embed.add_field(name=f'Attachment {iAttach}', value=attachment.url, inline=False)
                                # iAttach += 1
                                files.append(await attachment.to_file())
                        else:
                            bBot = True
                    if not bBot:
                        if len(files) > 0:
                            await channel.send(embed=embed, files=files)
                        else:
                            await channel.send(embed=embed)
        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                self.logger.critical(line)

    async def modifyMemberRoles(self, iRoles, guildId, member:discord.Member, bAdd):
        try:
            guild = self.get_guild(guildId)
            rolesToModify = []
            for iRole in iRoles:
                role = guild.get_role(iRole)
                if role:
                    rolesToModify.append(role)
            if len(rolesToModify) > 0:
                if bAdd:
                    await member.add_roles(*rolesToModify)
                else:
                    await member.remove_roles(*rolesToModify)
        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                self.logger.critical(line)

    async def on_raw_reaction_add(self, payload:discord.RawReactionActionEvent):
        settings = self.serverSettings.getServer(payload.guild_id)
        if payload.channel_id != settings.reactOnRoleChannel:
            return
        rolesToAdd = settings.getRolesOnApply(payload.emoji.name)
        await self.modifyMemberRoles(rolesToAdd, payload.guild_id, payload.member, True)

    async def on_raw_reaction_remove(self, payload:discord.RawReactionActionEvent):
        settings = self.serverSettings.getServer(payload.guild_id)
        if payload.channel_id != settings.reactOnRoleChannel:
            return
        rolesToRemove = settings.getRolesOnApply(payload.emoji.name)
        await self.modifyMemberRoles(rolesToRemove, payload.guild_id, payload.member, True)

    async def on_member_join(self, member: discord.Member):
        try:
            settings = self.serverSettings.getServer(member.guild.id)
            if settings.enhancedWelcomeLog:
                channel = self.get_channel(settings.welcomeLogChannel)
                if channel:
                    embed = discord.Embed(colour=discord.Colour.blurple(),
                                           title=f"{member.display_name} has joined")
                    embed.set_author(name=member.display_name, icon_url=member.display_avatar.url)
                    embed.add_field(name='Created On',
                                    value=f'{member.created_at.strftime(settings.welcomeLogStrFmt)}', inline=False)
                    embed.add_field(name=f'Joined {settings.serverName} On',
                                    value=f'{member.joined_at.strftime(settings.welcomeLogStrFmt)}',
                                    inline=False)
                    embed.add_field(name="Discord ID", value=member.id)
                    embed.set_footer(text="RogueBot", icon_url=self.user.display_avatar.url)
                    await channel.send(embed=embed)
        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                self.logger.critical(line)


    async def on_message(self, message:discord.Message):
        try:
            if self.userManagementModule:
                if await self.userManagementModule.checkIfSpambait(message):
                    return

            if message.author == self.user:
                return
            for user in message.mentions: # type: discord.Member
                if user.mention == self.user.mention:
                    await message.channel.typing()
                    dynaCommand = self.settings.onMention
                    if dynaCommand.ResponseType != EResponseType.NoResponse:
                        await self.runDynamicCommand(message, dynaCommand)
                    else:
                        await message.channel.send("Woof!")
                    return

            # print(f'msg: {message.content}')
            if message.author.id in SUPPORT_BOT_IDS:
                if "!support" in message.content:
                    if "!support" in self.settings.commands:
                        await message.channel.typing()
                        await message.channel.send(self.settings.commands["!support"].Response)
                        return

            try:
                stCmd = message.content.split()[0]
            except:
                stCmd = ''

            if stCmd in self.CommandMap:
                botCommand = self.CommandMap[stCmd]
                await botCommand.runCommand(message, self.serverSettings.getServer(message.guild.id))

            if message.content == "!bot":
                await message.channel.typing()
                await message.channel.send(f"Available Commands:\n{self.settings.generateHelpText()}")
                return

            if message.content == "!memes":
                await message.channel.typing()
                await message.channel.send(f"Available Meme Commands for your shitposting needs:\n{self.settings.generateMemesHelpText()}")
                return

            if message.content == "!modcmds":
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await message.channel.send(f"Available Moderator Commands:\n{self.settings.generateModHelpText()}")
                return

            if message.content == "!listguilds":
                try:
                    await message.channel.typing()
                except:
                    pass
                canRun = await self.canRunRestrictedCommand(message, self.OwnerLevelCmd)
                if canRun:
                    await self.listGuilds(message)
                return

            if message.content.startswith("!leaveguild"):
                try:
                    await message.channel.typing()
                except:
                    pass
                canRun = await self.canRunRestrictedCommand(message, self.OwnerLevelCmd)
                if canRun:
                    await self.leaveGuild(message)
                return

            if message.content == "!reload":
                try:
                    await message.channel.typing()
                except:
                    pass
                canRun = await self.canRunRestrictedCommand(message, self.RestrictedCmd)
                if canRun:
                    self.settings.reloadIfNeeded(True)
                    await message.channel.send(f"reloaded Commands, found {len(self.settings.commands)} dyna commands")

            if message.content == "!dlcmds":
                try:
                    await message.channel.typing()
                except:
                    pass
                canRun = await self.canRunRestrictedCommand(message, self.AdminOnlyCmd)
                if canRun:
                    if os.path.exists(BOTSETTINGSFILE):
                        dFile = discord.File(BOTSETTINGSFILE)
                        await message.channel.send(file=dFile)
                    else:
                        await message.channel.send("No Settings Found!")

            if message.content == "!dlsettings":
                try:
                    await message.channel.typing()
                except:
                    pass
                canRun = await self.canRunRestrictedCommand(message, self.OwnerLevelCmd)
                if canRun:
                    self.serverSettings.toFile(SERVERSETTINGSTMP)
                    if os.path.exists(SERVERSETTINGSTMP):
                        dFile = discord.File(SERVERSETTINGSTMP)
                        await message.channel.send(file=dFile)
                    else:
                        await message.channel.send("No Settings Found!")

            if message.content == '!dlavatar':
                try:
                    await message.channel.typing()
                except:
                    pass
                canRun = await self.canRunRestrictedCommand(message, self.AdminOnlyCmd)
                if canRun:
                    await message.channel.send(self.user.avatar.url)

            if message.content == "!dumpdb":
                try:
                    await message.channel.typing()
                except:
                    pass
                canRun = await self.canRunRestrictedCommand(message, self.OwnerLevelCmd)
                if canRun:
                    await self.dumpDb(message)
                return

            if message.content == "!importdb":
                try:
                    await message.channel.typing()
                except:
                    pass
                canRun = await self.canRunRestrictedCommand(message, self.OwnerLevelCmd)
                if canRun:
                    await self.importDb(message)
                return

            if message.content == "!oldyeller":
                try:
                    await message.channel.typing()
                except:
                    pass
                canRun = await self.canRunRestrictedCommand(message, self.OwnerLevelCmd)
                if canRun:
                    await self.shutdownBot(message)
                return

            if message.content.startswith("!dresolve") or message.content.startswith("!uresolve"):
                await message.channel.typing()
                settings = self.serverSettings.getServer(message.guild.id)
                self.DynaRestrictCmd.restrictedRoles = settings.moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await self.resolveUsers(message, message.content.startswith("!uresolve"))

            if message.content == "!update":
                try:
                    await message.channel.typing()
                except:
                    pass
                canRun = await self.canRunRestrictedCommand(message, self.AdminOnlyCmd)
                if canRun:
                    attachments = message.attachments
                    if len(attachments) >= 1:
                        await attachments[0].save(BOTSETTINGSFILE)
                        self.settings.reloadIfNeeded(True)
                        await message.channel.send(f"updated Commands, found {len(self.settings.commands)} dyna commands")
                    else:
                        await message.channel.send("message had no attachment!")

            if message.content == "!updatess":
                try:
                    await message.channel.typing()
                except:
                    pass
                canRun = await self.canRunRestrictedCommand(message, self.OwnerLevelCmd)
                if canRun:
                    attachments = message.attachments
                    if len(attachments) >= 1:
                        await attachments[0].save(SERVERSETTINGSFILE)
                        self.serverSettings.fromFile(SERVERSETTINGSFILE)
                        self.serverSettings.updateCaches()
                        await message.channel.send(f"updated Server Settings, found {len(self.serverSettings.servers)} servers")
                        await self.userManagementModule.initOnlyBans()
                    else:
                        await message.channel.send("message had no attachment!")

            if message.content == "!lfile":
                await message.channel.typing()
                canRun = await self.canRunRestrictedCommand(message, self.RestrictedCmd)
                if canRun:
                    files = os.listdir(FILESTORE)
                    ret = "Available Files:\n- "
                    ret += "\n- ".join(files)
                    await message.channel.send(ret)

            if message.content.startswith("!gfile"):
                await message.channel.typing()
                canRun = await self.canRunRestrictedCommand(message, self.RestrictedCmd)
                if canRun:
                    filename = self.RestrictedCmd.getExtras(message.content)
                    await self.sendFile(filename, message.channel)

            if message.content.startswith("!deletefile"):
                await message.channel.typing()
                canRun = await self.canRunRestrictedCommand(message, self.AdminOnlyCmd)
                if canRun:
                    filename = self.RestrictedCmd.getExtras(message.content)
                    try:
                        os.remove(os.path.join(FILESTORE, filename))
                        await message.channel.send("resource removed")
                    except:
                        await message.channel.send(f"Failed to remove resource: {filename}")

            if message.content.startswith("!afile"):
                await message.channel.typing()
                canRun = await self.canRunRestrictedCommand(message, self.AdminOnlyCmd)
                if canRun:
                    filename = self.RestrictedCmd.getExtras(message.content)
                    attachments = message.attachments
                    if len(attachments) >= 1:
                        await attachments[0].save(os.path.join(FILESTORE, filename))
                        await message.channel.send(f"added resource: {filename}")
                    else:
                        await message.channel.send("message had no attachment!")

            if message.content.startswith("!carchive"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await self.archiveChannel(message)

            if message.content.startswith("!larchive"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await self.listArchives(message)

            if message.content.startswith("!rarchive"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await self.removeArchive(message)

            if message.content.startswith("!garchive"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await self.getArchive(message)

            if message.content.startswith("!remindme") or message.content.startswith("!pokeme"):
                await self.createReminder(message)

            if message.content == "!raffle":
                await message.channel.typing()
                serverSetup = self.serverSettings.getServer(message.guild.id)
                if len(serverSetup.raffleEntryRoles) > 0:
                    for role in message.author.roles:
                        if role.id in serverSetup.raffleEntryRoles:
                            break
                    else:
                        await message.channel.send(embed=self.makeErrorEmbed("unable to enter raffle, current raffle is restricted"))
                        return
                if self.SavedData.addRaffler(message.author.mention, message.guild.id):
                    self.SavedData.toFile(SAVEDDATAFILE)
                    await message.channel.send(f"Thanks {message.author.mention}, you have entered the raffle")
                else:
                    await message.channel.send(f"you were already in the raffle")

            if message.content == "!rafflecount":
                await message.channel.typing()
                await message.channel.send(f"There are currently {self.SavedData.getRafflerCount(message.guild.id)} rafflers!")

            if message.content == "!rafflereset":
                await message.channel.typing()
                serverSetup = self.serverSettings.getServer(message.guild.id)
                if serverSetup is None:
                    self.RaffleCmd.restrictedRoles = []
                else:
                    self.RaffleCmd.restrictedRoles = serverSetup.raffleCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.RaffleCmd)
                if canRun:
                    self.SavedData.resetRafflers(message.guild.id)
                    self.SavedData.toFile(SAVEDDATAFILE)
                    await message.channel.send(f"Raffle has been reset!")

            if message.content == "!winner":
                await message.channel.typing()
                serverSetup = self.serverSettings.getServer(message.guild.id)
                if serverSetup is None:
                    self.RaffleCmd.restrictedRoles = []
                else:
                    self.RaffleCmd.restrictedRoles = serverSetup.raffleCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.RaffleCmd)
                if canRun:
                    if self.SavedData.getRafflerCount(message.guild.id) > 0:
                        winner = f"Winner is {self.SavedData.pickWinner(message.guild.id)}"
                        await message.channel.send(winner)
                    else:
                        await message.channel.send(f"Raffle has no entries!")

            if message.content == "!openticket":
                try:
                    await message.channel.typing()
                except:
                    pass
                server = message.channel.guild  # type: discord.Guild
                settings = self.serverSettings.getServer(server.id)
                for role in message.author.roles:
                    if role.id == settings.supportMuteRole:
                        await message.channel.send(f'sorry {message.author.mention}, you have been support muted and cannot make tickets')
                        await message.channel.send('https://tenor.com/view/no-soup-for-you-soup-no-soup-gif-11066779')
                        return

                if settings.ticketCat == "":
                    await message.channel.send(f'sorry ticket settings not configured for this guild')
                    return
                categories = server.categories
                searchingFor = settings.ticketCat
                supportChan = settings.ticketCat
                catCount = 1
                sFull = True
                targetCategory = None
                while sFull:
                    for cat in categories:
                        if cat.name.lower() == searchingFor:
                            if len(cat.channels) >= 50:
                                catCount += 1
                                searchingFor = supportChan + str(catCount)
                                break
                            else:
                                # no action needed
                                sFull = False
                                targetCategory = cat
                                break
                    else:
                        stMsg = ""
                        for role in server.roles:
                            if role.id in settings.pingOnTicketsFull:
                                stMsg += f"{role.mention}, "
                        if stMsg == "":
                            # roles dont exist here so no tickets
                            return
                        stMsg += f'Plx Halp, the ticket list is full please create: {settings.ticketCat}{catCount} for me. Thanks <3'
                        await message.channel.send(stMsg)
                        await message.channel.send("https://tenor.com/view/shitter-shitters-ful-snow-unclogging-gif-17419118")
                        return
                self.SavedData.incrementTicket(server.id)


                channel = await server.create_text_channel(f"{settings.ticketPrefix}-{self.SavedData.getTicketNo(server.id)}", category=targetCategory,
                                                           topic=f"This channel was created at {datetime.datetime.utcnow().isoformat()} by {message.author.display_name}.",
                                                           overwrites=self.getTicketPerms(server, message.author, self.serverSettings.getServer(server.id)))
                if settings.ticketMsgEmbed:
                    embed = discord.Embed(colour=settings.ticketMsgEmbedColour)
                    embed.description = settings.ticketMsg.replace('\n', '\n\n')
                    await channel.send(embed=embed)
                else:
                    await channel.send(settings.ticketMsg)
                await channel.send(message.author.mention)
                if len(settings.pingOnTicketRoles) > 0:
                    stMsg = ""
                    for role in server.roles:
                        if role.id in settings.pingOnTicketRoles:
                            stMsg += f"{role.mention}, "
                    await channel.send(stMsg)
                self.startSqlEntry()
                ticket = DiscordTicket()
                ticket.channelId = channel.id
                ticket.guildId = server.id
                ticket.userId = message.author.id
                ticket.ticketId = self.SavedData.getTicketNo(server.id)
                self.Session.add(ticket)
                self.Session.commit()
                await message.channel.send(f"Ticket opened: {channel.mention}")
                self.SavedData.toFile(SAVEDDATAFILE)

            if (message.content == "!close" or message.content.startswith("!closeticket")) and message.channel.name.lower().startswith(self.serverSettings.getServer(message.guild.id).ticketPrefix.lower()) and not message.channel.name.lower().endswith("closed"):
                server = message.channel.guild # type: discord.Guild
                settings = self.serverSettings.getServer(server.id)
                categories = server.categories
                catCount = 1
                sFull = True
                targetCategory = None
                for cat in categories:
                    if cat.name.lower() == settings.closedTicketCat:
                        targetCategory = cat
                        break
                if targetCategory is None:
                    # no place to move tickets!
                    await message.channel.send(f"Could not move the ticket! target category: {settings.closedTicketCat} not found")
                    return
                # await message.channel.edit(name=message.channel.name + "-closed", category=targetCategory)
                # await message.channel.edit(sync_permissions=True)
                # perms = self.getTicketPerms(server, None, settings)
                # for role in perms:
                #     await message.channel.set_permissions(role, overwrite=perms[role])
                await self.closeOrMoveTicket(message, targetCategory, settings, True)

            if (message.content == "!delete" or message.content == "!deleteticket") and message.channel.name.lower().startswith(self.serverSettings.getServer(message.guild.id).ticketPrefix.lower()) and message.channel.name.lower().endswith("closed"):
                serverSetup = self.serverSettings.getServer(message.guild.id)
                if serverSetup.ticketDeleteDelay > 0:
                    await message.channel.typing()
                    await message.channel.send(embed=self.makeSuccessEmbed(f"Ticket to be deleted in {serverSetup.ticketDeleteDelay} seconds"))
                    asyncio.create_task(self.delayedCommand(serverSetup.ticketDeleteDelay, message.channel.delete))
                else:
                    await message.channel.delete()

            if message.content.startswith("!warnuser"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await message.channel.send(await self.addUserFlag(message, EBanFlags.Warned, "warned", "!warnuser"))

            if message.content.startswith("!muteuser"):
                await message.channel.typing()
                serverSetup = self.serverSettings.getServer(message.guild.id)
                self.DynaRestrictCmd.restrictedRoles = serverSetup.moderatorCmdRoles + serverSetup.muteCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await message.channel.send(await self.addUserFlag(message, EBanFlags.Mute, "muted", "!muteuser"))

            if message.content.startswith("!unmuteuser"):
                await message.channel.typing()
                serverSetup = self.serverSettings.getServer(message.guild.id)
                self.DynaRestrictCmd.restrictedRoles = serverSetup.moderatorCmdRoles + serverSetup.muteCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await message.channel.send(await self.addUserFlag(message, EBanFlags.MuteDropped, "unmuted", "!unmuteuser"))

            if message.content.startswith("!smuteuser"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await message.channel.send(await self.addUserFlag(message, EBanFlags.SMute, "support-muted", "!smuteuser"))

            if message.content.startswith("!unsmuteuser"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await message.channel.send(await self.addUserFlag(message, EBanFlags.SMuteDropped, "uns-muted", "!unsmuteuser"))

            if message.content.startswith("!addreporter"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await message.channel.send(await self.addUserFlag(message, EBanFlags.Null, "added", "!addreporter"))

            if message.content.startswith("!modnote"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await message.channel.send(await self.addUserFlag(message, EBanFlags.ModNote, "note added", "!modnote"))

            if message.content.startswith("!removeuserrecord"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    try:
                        iId = int(message.content.split()[-1])
                    except:
                        await message.channel.send("couldn't parse Id")
                        return
                    await message.channel.send(self.removeUser(iId, self.serverSettings.getServer(message.guild.id).reporterGroup))

            if message.content == "!listreports":
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    msgs = self.listUsers(self.serverSettings.getServer(message.guild.id).reporterGroup)
                    for msg in msgs:
                        await message.channel.send(msg)
                        time.sleep(0.05)
                return

            if message.content.startswith("!userrecord"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                bDiscordId = False
                if canRun:
                    try:
                        iId = int(message.content.split()[-1])
                    except:
                        # await message.channel.send("couldn't parse Id")
                        try:
                            iId = message.mentions[0].id
                            bDiscordId = True
                        except:
                            await message.channel.send("couldn't parse Id")
                            return
                    await message.channel.send(self.getUserRecord(iId,bDiscordId, self.serverSettings.getServer(message.guild.id).reporterGroup))

            if message.content.startswith("!freeze"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await self.freeze_channel(message.channel)
                    return

            if message.content.startswith("!unfreeze"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await message.channel.edit(sync_permissions=True)
                    await message.channel.send("https://tenor.com/view/dab-reporter-gif-18923264")
                    return

            if message.content.startswith("!chancount"):
                await message.channel.typing()
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    channelCount = len(message.guild.channels)
                    embed = discord.Embed(colour=discord.Colour.blurple())
                    embed.description = f'This server currently has {channelCount} channels!'
                    await message.channel.send(embed=embed)


            if message.content == "!openappeal":
                await message.channel.typing()
                server = message.channel.guild  # type: discord.Guild
                settings = self.serverSettings.getServer(server.id)
                for role in message.author.roles:
                    if role.id == settings.supportMuteRole or role.id == settings.muteRole:
                        break
                else:
                    await message.channel.send(f'{message.author.mention}, you have nothing to appeal')
                    await message.channel.send('https://tenor.com/view/im-sorry-dave-im-afraid-i-cant-do-that-gif-12928790')
                    return

                categories = server.categories
                searchingFor = "appeals"
                supportChan = "appeals"
                catCount = 1
                sFull = True
                targetCategory = None
                while sFull:
                    for cat in categories:
                        if cat.name.lower() == searchingFor:
                            if len(cat.channels) >= 50:
                                catCount += 1
                                searchingFor = supportChan + str(catCount)
                                break
                            else:
                                # no action needed
                                sFull = False
                                targetCategory = cat
                                break
                    else:
                        stMsg = ""
                        for role in server.roles:
                            if role.id in settings.pingOnTicketsFull:
                                stMsg += f"{role.mention}, "
                        if stMsg == "":
                            # roles dont exist here so no tickets
                            return
                        stMsg += f'Plx Halp, the ticket list is full please create: appeals{catCount} for me. Thanks <3'
                        await message.channel.send(stMsg)
                        await message.channel.send("https://tenor.com/view/shitter-shitters-ful-snow-unclogging-gif-17419118")
                        return
                self.SavedData.incrementTicket(server.id)


                channel = await server.create_text_channel(f"RogueAppeal-{self.SavedData.getTicketNo(server.id)}", category=targetCategory,
                                                           topic=f"This channel was created at {datetime.datetime.utcnow().isoformat()} by {message.author.display_name}.",
                                                           overwrites=self.getTicketPerms(server, message.author, self.serverSettings.getServer(server.id), modsOnly=True))
                # await addUserFlag(message, EBanFlags.Appealed, "appeal opened", "!openappeal")
                await channel.send("You have requested to appeal a Mute, Support-Mute, Suspension. This is only for mutes, support-mutes and suspensions if you wish to appeal a ban contact the complaints dept <@99768920344363008>. Please calmly and respectfully state your case with any supporting evidence you have in your favor in this ticket for the RT Team to look over. We will request any further information we deem necessary to review your case. Please be patient with us as we decide your fate. \n\nAbusing the appeal process, may get warned or banned")
                await channel.send(message.author.mention)
                await message.channel.send(f"Appeal opened: {channel.mention}")
                self.SavedData.toFile(SAVEDDATAFILE)

            if message.content == "!closeappeal" and message.channel.name.lower().startswith("rogueappeal"):
                self.DynaRestrictCmd.restrictedRoles = self.serverSettings.getServer(message.guild.id).moderatorCmdRoles
                canRun = await self.canRunRestrictedCommand(message, self.DynaRestrictCmd)
                if canRun:
                    await message.channel.delete()
            try:
                strCmd = message.content.split()[0]
            except:
                # means someone just posted a meme or file....something without text
                return

            if stCmd == "!roll":
                await self.rolldice(message)
                return

            stMsg = message.content
            if strCmd in self.settings.restrictedAliases:
                dynaCommand = self.settings.commands[self.settings.restrictedAliases[strCmd]]
                if dynaCommand.canRun(message.author, OWNERS):
                    stMsg = stMsg.replace(stMsg, self.settings.restrictedAliases[strCmd])
                    strCmd = self.settings.restrictedAliases[strCmd]


            if strCmd in self.settings.aliases:
                stMsg = stMsg.replace(strCmd, self.settings.aliases[strCmd])
                strCmd = self.settings.aliases[strCmd]


            # print(self.settings.commands.keys())
            if strCmd in self.settings.commands:
                dynaCommand = self.settings.commands[strCmd]
                await self.runDynamicCommand(message, dynaCommand)
                return

            for command in self.settings.extrasNeeded:
                if stMsg.startswith(command.command):
                    if not command.canRun(message.author, OWNERS):
                        return
                    extras = command.getExtras(stMsg)
                    msg = command.Response + extras
                    await message.channel.typing()
                    for role in command.pingRoles:
                        await message.channel.send(role)
                    await message.channel.send(msg)
                    return

            msgLower = message.content.lower()
            if "pink" in msgLower and "mech" in msgLower and ("mission" in msgLower or "battle" in msgLower):
                if self.serverSettings.getServer(message.guild.id).allowPinkMechsText:
                    await message.channel.typing()
                    await message.channel.send("I think you experience completely pink mechs in combat? That is a known (vanilla) issue relating to the higher memory usage on those maps. Your mechs will be fine, but a game restart will sort of help.")

            if isinstance(message.channel, discord.DMChannel):
                stMsg = random.choice(self.settings.dmResponses)
                await message.channel.send(stMsg)


        except:
            trace = traceback.format_exc()
            lst_trace = trace.split('\n')
            for line in lst_trace:
                LOGGER.critical(line)


client = Roguebot(LOGGER)
client.run(TOKEN)