from .data import BaseDataObject, SubObjectMap
import random

random.seed()


class RafflerData(BaseDataObject):
    XML_Type = "RaffleData"

    def __init__(self):
        self.serverId = 0
        self.rafflers = []

    def addRaffler(self, raffler):
        if raffler not in self.rafflers:
            self.rafflers.append(raffler)
            return True
        return False

    def resetRafflers(self):
        self.rafflers = []

    def pickWinner(self):
        return random.choice(self.rafflers)


class ServerData(BaseDataObject):

    XML_Type = 'ServerData'

    def __init__(self):
        self.ticketNo = 0
        self.serverId = 0

class SavedData(BaseDataObject):

    XML_Type = 'SavedData'

    _SubObjectMapping = [
        SubObjectMap(RafflerData, '_rafflers', 'RaffleData'),
        SubObjectMap(ServerData, '_servers', 'ServerData')
    ]

    def __init__(self):
        self.ticketNo = 0
        self._servers = [] # type: list[ServerData]
        self._rafflers = [] # type: list[RafflerData]

    def getTicketNo(self, serverId):
        for server in self._servers:
            if server.serverId == serverId:
                return server.ticketNo
        server = ServerData()
        server.serverId = serverId
        self._servers.append(server)
        return server.ticketNo

    def incrementTicket(self, serverId):
        for server in self._servers:
            if server.serverId == serverId:
                server.ticketNo += 1
                return True
        server = ServerData()
        server.serverId = serverId
        self._servers.append(server)
        server.ticketNo = 1
        return False

    def getRafflerCount(self, server):
        for raffle in self._rafflers:
            if raffle.serverId == server:
                return len(raffle.rafflers)
        return 0

    def addRaffler(self, raffler, server):
        for raffle in self._rafflers:
            if raffle.serverId == server:
                return raffle.addRaffler(raffler)
        raffle = RafflerData()
        raffle.serverId = server
        raffle.addRaffler(raffler)
        self._rafflers.append(raffle)
        return True

    def resetRafflers(self, server):
        for raffle in self._rafflers:
            if raffle.serverId == server:
                return raffle.resetRafflers()

    def pickWinner(self, server):
        for raffle in self._rafflers:
            if raffle.serverId == server:
                return raffle.pickWinner()