from .data import BaseDataObject, SubObjectMap
from .dynamicresponse import DynamicResponse
from .eresponsetype import EResponseType
import os
import time


class BotSettings(BaseDataObject):

    XML_Type = 'BotSettings'

    _SubObjectMapping = [
        SubObjectMap(DynamicResponse, '_commands'),
        SubObjectMap(DynamicResponse, '_onMention', bol_list=False, str_xml_container_tag='onMention')
    ]

    def __init__(self):
        self._commands = [] # type: list[DynamicResponse]
        self._onMention = DynamicResponse()
        self._lastLoadTime = 0
        self._path = ''
        self._commandMap = {}
        self._extrasList = []
        self.nonDynaHelpText = {}
        self.modHelpTest = {}
        self._aliasMap = {}
        self._restrictedAliasMap = {}
        self.dmResponses = [
            "Test Response!"
        ]

    @property
    def onMention(self):
        return self._onMention

    @property
    def commands(self):
        """
        :return:
        :rtype: dict[str, DynamicResponse]
        """
        return self._commandMap

    @property
    def aliases(self):
        """
        :rtype: dict[str, str]
        """
        return self._aliasMap

    @property
    def restrictedAliases(self):
        """
        :rtype: dict[str, str]
        """
        return self._restrictedAliasMap

    @property
    def extrasNeeded(self):
        """
        :rtype: list[DynamicResponse]
        """
        return self._extrasList

    @property
    def Path(self):
        return self._path

    def setPath(self, sPath):
        self._path = sPath

    def reloadIfNeeded(self, bForce=False):
        if time.time() - self._lastLoadTime > 300 or bForce:
            if os.path.isfile(self._path):
                self.fromFile(self._path)
                self._lastLoadTime = time.time()
                self.rebuildCommandMap()
                print(f"Reloaded settings, found: {len(self.commands)}, dyna commands")
                return True
        return False

    def rebuildCommandMap(self):
        self._commandMap = {}
        self._aliasMap = {}
        self._restrictedAliasMap = {}
        self._extrasList = []
        for command in self._commands:
            if command.ResponseType == EResponseType.UseMessageExtras:
                self._extrasList.append(command)
            elif command.ResponseType == EResponseType.Alias:
                self._aliasMap[command.command] = command.Response
            elif command.ResponseType == EResponseType.RestrictedAlias:
                self._restrictedAliasMap[command.command] = command.Response
            else:
                self._commandMap[command.command] = command

    def generateHelpText(self):
        ret = ""
        commands = []
        for key in self.nonDynaHelpText:
            commands.append(key)
        for command in self._commands:
            if command.showInHelp:
                commands.append(command.command)

        commands = sorted(commands)
        for command in commands:
            if command in self._commandMap:
                tCommand = self._commandMap[command]
                ret += f"{tCommand.command} - {tCommand.helpText}\n"
            elif command in self.nonDynaHelpText:
                ret += f"{command} - {self.nonDynaHelpText[command]}\n"
        return ret

    def generateMemesHelpText(self):
        ret = ""
        commands = []
        for command in self._commands:
            if command.showInMemes:
                commands.append(command.command)

        commands = sorted(commands)
        for command in commands:
            if command in self._commandMap:
                tCommand = self._commandMap[command]
                ret += f"{tCommand.command} - {tCommand.helpText}\n"
        return ret

    def generateModHelpText(self):
        ret = ""
        commands = []
        for key in self.modHelpTest:
            commands.append(key)

        commands = sorted(commands)
        for command in commands:
            ret += f"{command} - {self.modHelpTest[command]}\n"
        return ret