from .data import BaseDataObject


class ReactForRole(BaseDataObject):

    XML_Type = 'ReactForRole'

    def __init__(self):
        self.emoji = ''
        self.roleIds = []