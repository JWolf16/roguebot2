from .DataObject import BaseDataObject
import xml.etree.cElementTree as ET
import base64
from math import ceil


class BitField(BaseDataObject):

    XML_Type = "BitField"

    def __init__(self, int_fieldsize=0, int_value=0):
        """
        Create a new BitField object

        :param int_fieldsize: the length of the bit field
        :type int_fieldsize: int
        :param int_value: the intial value of the bitField
        :type int_value: int
        """
        self._value = 0
        self.fieldSize = int_fieldsize
        self.maskField = 0

        for x in range(self.fieldSize):
            self.maskField = (self.maskField << 1) | 1

        if int_value is not None:
            self.setValue(int_value)

    def toXml(self):
        obj_xml = self._toXml()
        xml_tmp = ET.SubElement(obj_xml, 'fieldValue')
        xml_tmp.text = base64.standard_b64encode(str(self._value))
        return obj_xml

    def fromXml(self, obj_xml, bol_isXml=False):
        ret = self._fromXml(obj_xml, bol_isXml=bol_isXml)
        if obj_xml.find('fieldValue') is not None:
            self._value = int(base64.standard_b64decode(obj_xml.find('fieldValue').text))
        return ret


    @property
    def Value(self):
        """
        get the value of the bitfield

        :return: the value of the bitfield as an int
        :rtype: int
        """
        return self._value

    @property
    def hex(self):
        """
        get the value of the bitfield

        :return: a hex string
        :rtype: str
        """
        return '0x{0:0{1}X}'.format(self._value, int(self.fieldSize / 4))

    @property
    def bin(self):
        """
        get the value of the bit field

        :return: a binary string
        :rtype: str
        """
        return '{0:0{1}b}'.format(self._value, self.fieldSize)

    def setValue(self, int_value):
        """
        set the value of the bitfield

        :param int_value: the value to set, if larger than the allotted bits, it will be truncated
        :type int_value: int
        :return:
        :rtype:
        """
        self._value = (int_value & self.maskField)

    def setFromByteList(self, lst_bytes, bol_bigend=True):
        """
        set the value of the bit field form a list of bytes

        :param lst_bytes: the byte list
        :type lst_bytes: list[int]
        :param bol_bigend: when True (default) assume big endian byte order, when false use little endian byte order
        :type bol_bigend: bool
        :return:
        :rtype:
        """
        int_val = 0
        lst_bt = lst_bytes
        if not bol_bigend:  # if not big endian, reverse the list to put into little endian
            lst_bt = list(reversed(lst_bytes))
        for bt in lst_bt:
            int_val = ((int_val << 8) | bt)
        self.setValue(int_val)

    def toByteList(self, bol_bigend=True):
        """
        get the bit field value as a list of bytes

        :param bol_bigend: when True (default) return in big endian byte order, when False use little endian
        :type bol_bigend: bool
        :return:
        :rtype: list[int]
        """
        lst_bytes = []
        for x in range(int(ceil(self.fieldSize / 8.0))):
            int_val = ((self._value >> 8 * x) & 0xFF)
            lst_bytes.append(int_val)
        if bol_bigend:
            lst_bytes = list(reversed(lst_bytes))

        return lst_bytes

    def isSet(self, int_bit):
        """
        get if a particular bit is set

        :param int_bit: the bit to check (LSb is 0)
        :type int_bit: int
        :return: True if the bit is set, False otherwise
        :rtype: bool
        """
        if int_bit >= self.fieldSize:
            return False
        val = (self._value >> int_bit) & 0x1
        if val == 1:
            return True
        return False

    def _modify_bit(self, int_bit, bol_set):
        """
        modify a specific bit

        :param int_bit: the bit to modify
        :type int_bit: int
        :param bol_set: True to set the bit, False to unset it
        :type bol_set: bool
        :return: True on success, False otherwise
        :rtype: bool
        """
        if int_bit >= self.fieldSize:
            return False
        mask = 1 << int_bit
        self._value &= ~mask  # unset the bit
        if bol_set:
            self._value |= mask
        return True

    def modifyBit(self, iBit, bSet):
        """

        :param iBit:
        :type iBit: int
        :param bSet:
        :type bSet: bool
        :return:
        :rtype: bool
        """
        return self._modify_bit(iBit, bSet)

    def setBit(self, int_bit):
        """
        Set a specific bit

        :param int_bit: the bit to set
        :type int_bit: int
        :return: True on success, False otherwise
        :rtype: bool
        """
        return self._modify_bit(int_bit, True)

    def unSetBit(self, int_bit):
        """
        Unset a specific bit

        :param int_bit: the bit to unset
        :type int_bit: int
        :return: True on success, False otherwise
        :rtype: bool
        """
        return self._modify_bit(int_bit, False)

    def get_bit_struct(self, int_start_bit, int_length):
        """
        get a sub-structure of this bit field

        :param int_start_bit: the starting bit of this structure
        :type int_start_bit: int
        :param int_length: the length of this structure
        :type int_length: int
        :return: a new bitfield object that contains this sub-structure
        :rtype: BitField
        """
        val = self._value >> int_start_bit
        tmp = BitField(int_fieldsize=int_length)
        tmp.setValue(val)
        return tmp

    def getBitStruct(self, int_start_bit, int_length):
        return self.get_bit_struct(int_start_bit, int_length)

    def updateSubStruct(self, objStruct, iStartBit):
        """
        update a sub-section of the bitfield

        :param objStruct: the subsection to update
        :type objStruct: BitField
        :param iStartBit: the starting bit 0 = LSB, 31 = MSB
        :type iStartBit: int
        :return:
        :rtype:
        """
        for x in range(0, objStruct.fieldSize):
            self._modify_bit(iStartBit+x, objStruct.isSet(x))


class BitField4(BitField):

    def __init__(self, int_value=0):
        BitField.__init__(self, int_fieldsize=4, int_value=int_value)


class BitField8(BitField):

    def __init__(self, int_value=0):
        BitField.__init__(self, int_fieldsize=8, int_value=int_value)


class BitField16(BitField):

    def __init__(self, int_value=0):
        BitField.__init__(self, int_fieldsize=16, int_value=int_value)


class BitField32(BitField):

    def __init__(self, int_value=0):
        BitField.__init__(self, int_fieldsize=32, int_value=int_value)


class BitField64(BitField):

    def __init__(self, int_value=0):
        BitField.__init__(self, int_fieldsize=64, int_value=int_value)
