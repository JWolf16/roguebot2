import datetime


class ETimeType:

    Minutes = 'Minutes'
    Hours = 'Hours'
    Days = 'Days'
    Weeks = 'Weeks'

    TMap = {
        Minutes : Minutes,
        Hours : Hours,
        Days : Days,
        Weeks : Weeks,
        'm' : Minutes,
        'h' : Hours,
        'd' : Days,
        'w' : Weeks,
        Minutes.lower(): Minutes,
        Hours.lower(): Hours,
        Days.lower(): Days,
        Weeks.lower(): Weeks,
    }

    MaxTimeMap = {
        Minutes : 120,
        Hours : 168,
        Days: 45,
        Weeks: 16
    }

    MaxDiscordTimeOut = {
        Minutes: 40320,
        Hours: 672,
        Days: 28,
        Weeks: 4
    }

    @staticmethod
    def getDeltaMod(modifier:str):
        mod = modifier.lower()
        if mod in ETimeType.TMap:
            return ETimeType.TMap[mod]
        return None

    @staticmethod
    def timeOk(amount:int, mod:str, timeMap):
        if mod in timeMap:
            if amount <= timeMap[mod]:
                return True
        return False

    @staticmethod
    def getTimeDelta(amount:int, mod:str):
        if mod == ETimeType.Minutes:
            return datetime.timedelta(minutes=amount)
        elif mod == ETimeType.Hours:
            return datetime.timedelta(hours=amount)
        elif mod == ETimeType.Days:
            return datetime.timedelta(days=amount)
        elif mod == ETimeType.Weeks:
            return datetime.timedelta(weeks=amount)
        return None
