import xml.etree.cElementTree as ET
from xml.dom import minidom  # for pretty xml generation
import inspect
import ast
import base64
from io import BytesIO
import io
import os.path
import zipfile
import json
import codecs
import traceback
from collections import OrderedDict

import sys

BOL_PY3K = (sys.version_info[0] == 3)
BOL_WINREG = False

if BOL_PY3K:
    # These concepts dont exist in python 3 since all strings are unicode internally
    # and bytes & str no longer share enough functionality to share a base class
    # to keep py 2 compatibility just remap these to str functions
    unicode = str
    basestring = str
    from io import StringIO
else:
    from cStringIO import StringIO

# winreg is only available on windows
try:
    if BOL_PY3K:
        from winreg import HKEY_CURRENT_USER, ConnectRegistry, OpenKey, SetValueEx, KEY_ALL_ACCESS, REG_BINARY, QueryValueEx, CreateKey, HKEY_LOCAL_MACHINE
    else:
        from _winreg import HKEY_CURRENT_USER, ConnectRegistry, OpenKey, SetValueEx, KEY_ALL_ACCESS, REG_BINARY, QueryValueEx, CreateKey, HKEY_LOCAL_MACHINE
    BOL_WINREG = True
except ImportError:
    HKEY_CURRENT_USER = 'ThisKeyDoesntMatterForLinux'


class SubObjectMap(object):

    def __init__(self, cls_subobject, str_containing_memeber, str_xml_container_tag=None, bol_list=True):
        """

        :param cls_subobject: a class object, the class that the sub memeber should invoke
        :type cls_subobject: object
        :param str_containing_memeber: the member of this class that will contain the subclass, must a list type
        :type str_containing_memeber: str
        :param str_xml_container_tag: an XML subcontainer tag for the objects to reside in when in XML form
        :type str_xml_container_tag: str
        :param bol_list: True if the container is a list, False if its a regular member
        :type bol_list: bool
        """
        self.SubClass = cls_subobject
        self.Container = str_containing_memeber
        self.XmlContainer = str_xml_container_tag
        # Json must have a sub container
        self.JsonContainer = None
        self.isList = bol_list
        if self.XmlContainer is not None:
            self.JsonContainer = self.XmlContainer
        else:
            self.JsonContainer = self.SubClass.XML_Type


class BaseDataObject(object):
    """
     A basic data class that can auto-generate itself into forms useful for printing or writing to file

    .. warning::
        Don't ever invoke on its own, think of like a c++-style virtual class. This class is meant to
        be sub-classed into objects for specific data types and provide those auto-generating capabilities to them

    """
    XML_Type = 'BaseDataObject'  #: XML Root Rag for this object
    _ExcludeFromMemebers = []  #: a list of members to exclude from auto-generate functions
    _StrShowProps = False       #: Force a string representation to show its properties when true
    _SubObjectMapping = []
    _B64Memebers = []  #: Memebers that need to be base64 encoded for serialization as they may hold invalid xml data
    _useStdDict = True

    # Windows Registry Info
    _BaseRegDatabase = HKEY_CURRENT_USER  #: the top level key, this can be changed depending on the needs, Current_user will keep settings for that user (things like preferences good here), Local_machine is better for system wide settings
    _BaseKeyPath = 'Software\\Christie\\TestAutomation\\'  #: the key path, best to leave this as is so all our keys are in one place
    _RegKey = 'YouShouldSetThis' #: This is the final bit of the path, ideally it is the name of the application this data is for (in regedit this is a directory)
    _RegValue = 'YouShouldAlsoSetThis' #: the value within the key to set, this should be unique to any other object used in the same application (in regedit these look item items or files)

    def _get_members(self, bol_include_props=False):
        """
        get all non-private member attributes

        .. note::
            if you wish to exclude a non-private member, add that member to :attr:`_ExcludeFromMemebers`

        :return: a list of all members that are not private, functions or properties
        :rtype: list
        """
        lst_members = inspect.getmembers(self)
        lst_filtered_members = []
        for member in lst_members:
            # print member
            if member[0].startswith(
                    '__'):  # remove all members that use the standard double underscore, this removes most of the default python object members
                pass
            elif member[0].startswith('_'):  # remove private members that we declare
                pass
            elif callable(getattr(self, member[0])):  # remove any member that is callable, we only want attributes
                pass
            elif isinstance(getattr(type(self), member[0], getattr(self, member[0])),
                            property):  # exclude properties because they are just for readability
                if bol_include_props:
                    lst_filtered_members.append(member)
            elif member[0] == 'XML_Type':  # remove the attributes from this base class so that only the ones added by child classes remain
                pass
            elif member[0] in self._ExcludeFromMemebers:
                pass
            else:
                lst_filtered_members.append(member)
        return lst_filtered_members

    def _toDct(self):
        """
        create a dictionary with attribute names as keys and their values as the value

        :return: a dictionary representation of this object
        :rtype: dict
        """
        lst_members = self._get_members()
        dct_to_return = {}
        for member in lst_members:
            dct_to_return[member[0]] = member[1]
        return dct_to_return

    def _fromDct(self, dct_data):
        """
        populate this object from a dictionary created by :meth:`_toDct`

        :param dct_data: a dictionary representation of this object
        :type dct_data: dict
        :return: None
        :rtype: None
        """

        lst_members = self._get_members()
        for member in lst_members:
            if member[0] in dct_data:
                setattr(self, member[0], dct_data[member[0]])

    ## <summary> make a nice printing string </summary>
    def __str__(self):
        lst_members = self._get_members(bol_include_props=self._StrShowProps)
        str_to_return = ''
        for member in lst_members:
            if member[1] is None:
                str_to_return += member[0] + ' : \n'
            else:
                str_to_return += member[0] + ' : ' + unicode(member[1]) + '\n'
        return str_to_return

    ## <summary> make a nice printing string </summary>
    def __unicode__(self):
        lst_members = self._get_members(bol_include_props=self._StrShowProps)
        str_to_return = u''
        for member in lst_members:
            if member[1] is None:
                str_to_return += unicode(member[0]) + u' : \n'
            elif isinstance(getattr(type(self), member[0], getattr(self, member[0])), basestring):
                str_to_return += unicode(member[0]) + u' : ' + member[1] + u'\n'
            else:
                str_to_return += unicode(member[0]) + u' : ' + unicode(member[1]) + u'\n'
        return str_to_return

    def __eq__(self, other):
        """
        define a base equality operator
        :return:
        :rtype: bool
        """

        lst_members = self._get_members()
        if not hasattr(other, 'XML_Type'):
            return False
        if self.XML_Type != other.XML_Type:
            return False
        for member in lst_members:
            if hasattr(other, member[0]):
                if getattr(other, member[0]) != member[1]:
                    break
            else:
                break
        else:
            return True
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def diff(self, other):
        """
        create a diff between two objects

        :param other: the object to be diffed against
        :type other: BaseDataObject
        :return: a dictionary with keys as the differing members and values as a 2 element list, the first is the value of this object, the second is the value of the other object
        :rtype: dict[str, list]
        """
        dct_diff = {}
        lst_members = self._get_members()
        if not hasattr(other, 'XML_Type'):
            return {'ObjectType': ['BaseDataObject', 'Not a BaseDataObject']}  # can't compare
        if self.XML_Type != other.XML_Type:
            return {'XML_Type': [self.XML_Type, other.XML_Type]}  # a comparision does not make sense
        for member in lst_members:
            if hasattr(other, member[0]):
                if getattr(other, member[0]) != member[1]:
                    dct_diff[member[0]] = [member[1], getattr(other, member[0])]
            else:
                dct_diff[member[0]] = [member[1], 'Member is missing']
        return dct_diff

    def _toXml(self):
        """
        make an xml representation of this object

        :return: an XML representation of the object
        :rtype: XML Element
        """

        lst_members = self._get_members()
        xml_obj = ET.Element(self.XML_Type)
        for member in lst_members:
            xml_tmp = ET.SubElement(xml_obj, member[0])
            if member[1] is None:
                xml_tmp.text = ''
            else:
                member_data = member[1]
                if member[0] in self._B64Memebers:
                    xml_tmp.text = base64.standard_b64encode(member_data)
                elif isinstance(member_data, OrderedDict):
                    member_data = dict(member_data)
                    member_data = unicode(member_data)
                    xml_tmp.text = member_data
                else:
                    try:
                        member_data = unicode(member_data)
                    except UnicodeDecodeError:  # Handle Python 2's terrible unicode handling, if there are utf-8 characters in the original string python will throw
                                                # a unicode decode error, even though we are telling it to treat the string as unicode
                        member_data = unicode(member_data, encoding='utf-8')
                    if member_data == 'True':
                        xml_tmp.text = 'true'
                    elif member_data == 'False':
                        xml_tmp.text = 'false'
                    else:
                        xml_tmp.text = member_data
        for item in self._SubObjectMapping:
            xml_tmp = xml_obj
            if item.XmlContainer is not None:
                xml_tmp = ET.SubElement(xml_obj, item.XmlContainer)
            member = getattr(self, item.Container)
            if item.isList:
                for obj in member:
                    xml_tmp.append(obj.toXml())
            else:
                xml_tmp.append(member.toXml())
        return xml_obj

    @staticmethod
    def make_xml(str_xml, bol_throw_except=False):
        """
        create and return an ElementTree element object from a valid xml string

        :param str_xml: an xml string
        :type str_xml: str
        :return: an XML Element object
        :rtype: XML Element
        """
        if not isinstance(str_xml, basestring):
            return str_xml
        el_root = None
        try:
            obj_file = StringIO(str_xml)
            obj_file.seek(0)
            el_root = ET.parse(obj_file).getroot()
        except Exception as e:
            if bol_throw_except:
                raise
        return el_root

    def _fromXml(self, obj_xml, bol_isXml=False):
        """
        populate this object from an xml representation of the object

        :param obj_xml: the xml representation, either as an ElementTree element object or as a string
        :param bol_isXml: when True obj_xml is an ElementTree element object, when False it is a string
        :type bol_isXml: bool
        :return: True if successful, False otherwise
        :rtype: bool
        """
        el_root = None
        if not bol_isXml:
            try:
                el_root = self.make_xml(obj_xml, bol_throw_except=True)
            except Exception as e:
                return False
        else:
            el_root = obj_xml
        if el_root.tag != self.XML_Type:
            return False

        lst_members = self._get_members()
        for member in lst_members:
            xml_tmp = el_root.find(member[0])
            if xml_tmp is not None:
                try:
                    if xml_tmp.text is not None:
                        if member[0] in self._B64Memebers:
                            setattr(self, member[0], base64.standard_b64decode(xml_tmp.text))
                        else:
                            if xml_tmp.text == 'true':
                                setattr(self, member[0], True)
                            elif xml_tmp.text == 'false':
                                setattr(self, member[0], False)
                            elif xml_tmp.text.lower() == 'none':
                                setattr(self, member[0],
                                        unicode(xml_tmp.text))  # if the string is None ast will treat it as a NoneType, we don't want that
                            else:
                                setattr(self, member[0], ast.literal_eval(xml_tmp.text))
                except (ValueError, SyntaxError):
                    if xml_tmp.text is not None:
                        setattr(self, member[0], unicode(xml_tmp.text))  # for strings I guess?
                except Exception as e:
                    print(str(e))
                    print(xml_tmp.text)
        for item in self._SubObjectMapping:
            obj_tmp = el_root
            if item.XmlContainer is not None:
                obj_tmp = el_root.find(item.XmlContainer)

            # Set the containing list as empty
            if item.isList:
                setattr(self, item.Container, [])
                obj_container = getattr(self, item.Container)

                if obj_tmp is not None:

                    for tag in obj_tmp.findall(item.SubClass.XML_Type):
                        new_obj = item.SubClass()
                        new_obj.fromXml(tag)
                        obj_container.append(new_obj)
            else:
                new_obj = item.SubClass()
                if obj_tmp.find(item.SubClass.XML_Type) is not None:
                    new_obj.fromXml(obj_tmp.find(item.SubClass.XML_Type))
                setattr(self, item.Container, new_obj)

        return True

    def _fromJson(self, dct_json):
        """
        populate this object from a json string representation of the object

        :param dct_json: the json representation
        :type dct_json: OrderedDict
        :return: True if successful, False otherwise
        :rtype: bool
        """

        lst_members = self._get_members()
        for member in lst_members:
            if member[0] in dct_json:
                try:
                    if member[0] in self._B64Memebers:
                        setattr(self, member[0], base64.standard_b64decode(dct_json[member[0]]))
                    else:
                        setattr(self, member[0], dct_json[member[0]])
                except Exception as e:
                    return False
        for item in self._SubObjectMapping:
            # Set the containing list as empty

            obj_tmp = None
            if not item.isList:
                obj_tmp = dct_json
            if item.JsonContainer is not None and item.isList:
                if item.JsonContainer in dct_json:
                    obj_tmp = dct_json[item.JsonContainer]

            if item.isList:
                setattr(self, item.Container, [])
                obj_container = getattr(self, item.Container)

                if obj_tmp is not None:
                    for tag in obj_tmp:
                        new_obj = item.SubClass()
                        new_obj._fromJson(tag)
                        obj_container.append(new_obj)
            else:
                new_obj = item.SubClass()
                if item.JsonContainer in obj_tmp:
                    new_obj._fromJson(obj_tmp[item.JsonContainer])
                setattr(self, item.Container, new_obj)

        return True

    def _toJson(self):
        """
        make an json representation of this object

        :return: an JSON representation of the object
        :rtype: OrderedDict
        """

        lst_members = self._get_members()
        dct_json = OrderedDict()
        if self._useStdDict:
            dct_json = {}
        for member in lst_members:
            if member[1] is None:
                pass
            else:
                member_data = member[1]
                if member[0] in self._B64Memebers:
                    dct_json[member[0]] = base64.standard_b64encode(member_data)
                else:
                    dct_json[member[0]] = member[1]
        for item in self._SubObjectMapping:
            json_tmp = item.SubClass.XML_Type
            if item.JsonContainer is not None:
                dct_json[item.JsonContainer] = []
                json_tmp = item.JsonContainer
            member = getattr(self, item.Container)
            if item.isList:
                for obj in member:
                    dct_json[json_tmp].append(obj._toJson())
            else:
                if member is not None:
                    dct_json[json_tmp] = member._toJson()
        return dct_json

    @classmethod
    def isOfType(cls, other):
        """
        identify if an object belongs to this data type

        :param other: the object to test
        :type other: object
        :return: True if the object is of this class, False otherwise
        :rtype: bool

        .. note::
            You dont need to create an instance to call this method

        """
        if not hasattr(other, 'XML_Type'):
            return False
        if cls.XML_Type == other.XML_Type:
            return True
        return False

    @staticmethod
    def make_pretty_xml(el_tree):
        """
        Convert the ugly xml generated by the ETree Parser to a pretty printed form

        :param el_tree: an XML object to be converted to a pretty printed xml string
        :type el_tree: XML Element
        :return: The xml in a 'pretty print' version
        :rtype: str
        """
        str_tmp = ET.tostring(el_tree, 'utf-8')
        xml_nicer = minidom.parseString(str_tmp)
        return xml_nicer.toprettyxml()

    def toXml(self):
        """
        make an xml representation of this object

        :return: an XML representation of the object
        :rtype: XML Element
        """
        return self._toXml()

    def fromXml(self, obj_xml, bol_isXml=False):
        """
        populate this item from an XML representation

        :param obj_xml: the XML either as an XML element or a string
        :type obj_xml: XML | str
        :param bol_isXml: True if obj_xml is an xml element, False otherwise
        :type bol_isXml: bool
        :return: True if successful, False otherwise
        :rtype: bool
        """
        return self._fromXml(obj_xml, bol_isXml=bol_isXml)

    def toJson(self, bol_pretty=False):
        """
        make a json representation of this object
        :param bol_pretty: whether to pretty print the result
        :type bol_pretty: bool
        :return: an json representation of the object
        :rtype: str
        """
        data =  self._toJson()
        if bol_pretty:
            return json.dumps(data, indent=4, ensure_ascii=False)
        return json.dumps(data, ensure_ascii=False)

    def toRawJson(self):
        return self._toJson()

    def fromJson(self, str_json):
        """
        populate this item from a json representation

        :param str_json: the json representation as a string
        :type str_json: str
        :return: True if successful, False otherwise
        :rtype: bool
        """
        dct_json = None
        if isinstance(str_json, dict) or isinstance(str_json, OrderedDict):
            dct_json = str_json
        else:
            try:
                if self._useStdDict:
                    dct_json = json.loads(str_json)
                else:
                    dct_json = json.loads(str_json, object_pairs_hook=OrderedDict, object_hook=OrderedDict)
            except Exception as e:
                print(traceback.format_exc())
                return False
        if dct_json is None:
            return False
        return self._fromJson(dct_json)

    def toBase64(self):
        """
        create a base64 encoded string representation of this object

        :return: a base64 encoded string
        :rtype: str
        """
        return base64.standard_b64encode(ET.tostring(self.toXml(), 'utf-8'))

    def fromBase64(self, data):
        """
        decode base64

        :return:
        :rtype:
        """
        return self.fromXml(base64.standard_b64decode(data).decode())

    def toZip(self, str_name):
        """
        create a zip file that contains the xml file of this object

        :param str_name: the name of the file to be created, with no extension
        :type str_name: str
        :return:
        :rtype:
        """
        obj_xml = self.toXml()
        obj_zip = zipfile.ZipFile(str_name + '.zip', 'w', compression=zipfile.ZIP_DEFLATED)
        obj_zip.writestr(os.path.basename(str_name) + '.xml', self.make_pretty_xml(obj_xml))
        obj_zip.close()

    def toFile(self, str_name):
        """
        write the current details to file

        :param str_name: the name of the file to be created
        :type str_name: str
        :return:
        :rtype:
        """
        str_data = ''
        if str_name.lower().endswith('.json'):
            str_data = self.toJson(True)
        else:
            str_data = self.make_pretty_xml(self.toXml())
        if BOL_PY3K:
            str_data = str_data.encode('utf-8')
        m_file = io.open(str_name, mode='wb')
        m_file.write(str_data)
        m_file.flush()
        m_file.close()

    def fromFile(self, str_name):
        """
        load data from file
        :param str_name: the name of the file to read from
        :type str_name: str
        :return:
        :rtype:
        """
        m_file = io.open(str_name, 'rb')
        data = m_file.read()
        if BOL_PY3K:
            data = data.decode('utf-8')
        m_file.close()
        if str_name.lower().endswith('.json'):
            return self.fromJson(data)
        else:
            self.fromXml(data)

    def _check_key(self):
        """
        check if the key is already in the registry and create it if its not
        :return:
        :rtype:
        """
        if BOL_WINREG:
            try:
                Reg = ConnectRegistry(None, self._BaseRegDatabase)
                try:
                    Key = OpenKey(Reg, self._BaseKeyPath + self._RegKey)
                except (WindowsError, OSError) : # key doesnt exist so create it
                    CreateKey(Reg, self._BaseKeyPath + self._RegKey)
                    Key = OpenKey(Reg, self._BaseKeyPath + self._RegKey, 0, KEY_ALL_ACCESS)
                    if BOL_PY3K:
                        SetValueEx(Key, self._RegValue, 0, REG_BINARY, self.make_pretty_xml(self.toXml()).encode('utf-8'))
                    else:
                        SetValueEx(Key, self._RegValue, 0, REG_BINARY, self.make_pretty_xml(self.toXml()))
            except Exception as e:
                # print(traceback.format_exc())
                pass
        else:
            return

    def loadReg(self):
        """
        load data from the registry

        ..note::
            This only works on windows systems

        :return: True on success, False otherwise
        :rtype: bool
        """
        if BOL_WINREG:
            self._check_key()
            try:
                Reg = ConnectRegistry(None, self._BaseRegDatabase)
                Key = OpenKey(Reg, self._BaseKeyPath + self._RegKey)
                data = QueryValueEx(Key, self._RegValue)[0]
                if BOL_PY3K:
                    data = data.decode('utf-8')
                return self.fromXml(data)
            except Exception as e:
                # print(traceback.format_exc())
                return False
        else:
            return False

    def saveReg(self):
        """
        save data to the registry

        ..note::
            This only works on windows systems

        :return: True on success, False otherwise
        :rtype: bool
        """
        if BOL_WINREG:
            self._check_key()
            data = self.make_pretty_xml(self.toXml())
            try:
                Reg = ConnectRegistry(None, self._BaseRegDatabase)
                Key = OpenKey(Reg, self._BaseKeyPath + self._RegKey, 0, KEY_ALL_ACCESS)
                if BOL_PY3K:
                    SetValueEx(Key, self._RegValue, 0, REG_BINARY, data.encode('utf-8'))
                else:
                    SetValueEx(Key, self._RegValue, 0, REG_BINARY, data)
                return True
            except Exception as e:
                # print(traceback.format_exc())
                return False
        else:
            return False