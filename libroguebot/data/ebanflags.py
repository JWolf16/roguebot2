
class EBanFlags(object):

    Warned = 0
    Mute = 1
    SMute = 2
    Banned = 3
    Appealed = 4
    MuteDropped = 5
    SMuteDropped = 6
    ModNote = 7

    Null = -1