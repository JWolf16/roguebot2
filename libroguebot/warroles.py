from .data import BaseDataObject, SubObjectMap
import os
import time


class WarRoles(BaseDataObject):

    XML_Type = 'WarRoles'

    def __init__(self):
        self.warRoles = []
        self.mercRole = 0
        self.serverMap = {}