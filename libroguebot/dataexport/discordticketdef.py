from libserdes import ISerDes


class DiscordTicketDef(ISerDes):

    _ContainerTag = "DiscordTicketDef"

    def __init__(self):
        self.userId = 0
        self.ticketId = 0
        self.channelId = 0
        self.guildId = 0
        self.alerted = False
