import datetime

from libserdes import ISerDes, SerDesSubObject

from .userhistorydef import UserHistoryDef
from .archiverecorddef import ArchiveRecordDef
from .discordticketdef import DiscordTicketDef
from .reminderdef import ReminderDef


class DbDumpDef(ISerDes):

    _ContainerTag = 'RbDbDump'

    _SubObjects = [
        SerDesSubObject(ArchiveRecordDef, '_archives', 'Archives'),
        SerDesSubObject(ReminderDef, '_reminders', 'Reminders'),
        SerDesSubObject(DiscordTicketDef, '_tickets', 'Tickets'),
        SerDesSubObject(UserHistoryDef, '_modHistory', 'ModHistory')
    ]

    def __init__(self):
        self.date = datetime.datetime.utcnow().ctime()
        self._archives = []
        self._reminders = []
        self._tickets = []
        self._modHistory = []

    @property
    def archives(self):
        return self._archives

    @property
    def reminders(self):
        return self._reminders

    @property
    def tickets(self):
        return self._tickets

    @property
    def modHistory(self):
        return self._modHistory