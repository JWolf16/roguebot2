from libserdes import ISerDes


class ReminderDef(ISerDes):

    _ContainerTag = 'ReminderDef'

    def __init__(self):
        self.userId = 0
        self.mentions = ""
        self.remindTime = 0
        self.channelId = 0
        self.guildId = 0
        self.reminderText = ""