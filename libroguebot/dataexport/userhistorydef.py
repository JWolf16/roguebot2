from libserdes import ISerDes, SerDesSubObject


class HistoryEntryDef(ISerDes):

    _ContainerTag = 'HistoryEntryDef'

    def __init__(self):
        self.banFlags = 0
        self.comments = ""
        self.timeStamp = 0
        self.reporter = ""


class UserHistoryDef(ISerDes):

    _ContainerTag = 'UserHistoryDef'

    _SubObjects = [
        SerDesSubObject(HistoryEntryDef, '_history', 'History')
    ]

    def __init__(self):
        self._history = [] # type: list[HistoryEntryDef]
        self.userId = 0
        self.mentions = ""
        self.userName = ""
        self.notes = ""
        self.banFlags = 0
        self.ServerGroup = 0

    @property
    def history(self):
        return self._history
