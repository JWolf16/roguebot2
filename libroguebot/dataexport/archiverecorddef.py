from libserdes import ISerDes


class ArchiveRecordDef(ISerDes):

    _ContainerTag = "ArchiveRecordDef"

    def __init__(self):
        self.guildId = 0
        self.fileName = ""
        self.channelName = ""
        self.channelId = 0
        self.timestamp = 0