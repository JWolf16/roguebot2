import discord
import json
import logging
import os
import os.path
from io import BytesIO
import traceback
import requests
import asyncio
import time

from .. import BotSettings, ServerSettings


class BaseModule:

    def __init__(self, logger: logging.Logger, settings: BotSettings, serverSettings: ServerSettings, db, roguebot: discord.Client, storeDir):
        self.logger = logger
        self.settings = settings
        self.serverSettings = serverSettings
        self.rbBot = roguebot
        self.storeDir = storeDir
        self.userAgent = 'Roguebot'
        self.db = db

    def startSqlEntry(self):
        """
        Make the SQL connection work, MySQL drops connections after long periods of inactivity, in theory SQLAlchemy's
        settings should handle this, but for whatever reason they dont seem to, so screw it and do it this way


        :return:
        :rtype:
        """
        try:
            self.db.commit()
        except:
            self.db.rollback()
            self.db.commit()

    @property
    def user(self):
        return self.rbBot.user


    def parseMsg(self, message: discord.Message):
        ltMsg = message.content.split()
        if len( ltMsg) == 2:
            stArg = ltMsg[1].strip('\n')

            return stArg
        elif len(ltMsg) > 2:
            return ltMsg[1:]
        else:
            return None

    async def chunkMsgs(self, stMsg, message: discord.Message, codeblock, chunkedChar=None):
        breakChar = ' '
        if chunkedChar:
            breakChar = chunkedChar
        if len(stMsg) >= 1800:
            idx = 1500
            while idx < len(stMsg) and idx < 1800:
                if stMsg[idx] == breakChar:
                    break
                idx += 1
            msg = stMsg[:idx]
            if codeblock:
                msg = msg.replace('```', '')
                msg = f'```{msg}```'
            await message.channel.send(msg)
            time.sleep(0.1)
            await self.chunkMsgs(stMsg[idx:], message, codeblock)
        else:
            msg = stMsg
            if codeblock:
                msg = stMsg.replace('```', '')
                msg = f'```{msg}```'
            await message.channel.send(msg)

    def getEnabled(self, stText):
        return stText.lower() in ['y', 'yes', 'true', 'on', 'enable', 'enabled']

    def makeErrorEmbed(self, msg):
        embed = discord.Embed(colour=discord.Colour.brand_red())
        embed.description = msg
        embed.set_footer(text=self.userAgent, icon_url=self.user.display_avatar.url)
        return embed

    def makeSuccessEmbed(self, msg):
        embed = discord.Embed(colour=discord.Colour.brand_green())
        embed.description = msg
        embed.set_footer(text=self.userAgent, icon_url=self.user.display_avatar.url)
        return embed

    def makeInformationalEmbed(self, msg):
        embed = discord.Embed(colour=discord.Colour.dark_purple())
        embed.description = msg
        embed.set_footer(text=self.userAgent, icon_url=self.user.display_avatar.url)
        return embed

    async def delayedCommand(self, delay, command):
        await asyncio.sleep(delay)
        await command()

    async def sendFile(self, filename, channel, data=None):

        resource = os.path.join(self.storeDir, filename)
        if data:
            bio = BytesIO()
            bio.write(data)
            bio.seek(0)
            dFile = discord.File(bio, filename=filename)
            await channel.send(file=dFile)
            return
        if os.path.exists(resource):
            dFile = discord.File(resource)
            await channel.send(file=dFile)
        else:
            await channel.send("Resource not found!")
