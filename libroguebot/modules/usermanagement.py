import traceback
import datetime

from .basemodule import BaseModule
from ..model import OnlyBansChannel, OnlyBansSubscribers
from ..serversettings import ServerSetup
import discord


class UserManagementModule(BaseModule):

    async def checkIfSpambait(self, message: discord.Message):
        if message.author.id == self.rbBot.user.id or message.guild is None:
            return False
        serverSetup = self.serverSettings.getServer(message.guild.id)
        if self.checkForSpamMessage(message, serverSetup):
            return await self.checkRoleAndBans(message, serverSetup, True)
        if message.channel.id in self.serverSettings.onlyBansChannels:
            self.logger.info("Detected new potential onlybans subscriber!")
            return await self.checkRoleAndBans(message, serverSetup, False)
        else:
            return False

    def checkForSpamMessage(self, message: discord.Message, serverSetup: ServerSetup):
        content = message.content.lower()
        criteriaMatched = 0
        termsMatched = 0
        massPings = 0
        for term in serverSetup.onlyBansTerms:
            if term in content:
                termsMatched += 1
        if termsMatched >= 1:
            criteriaMatched += 1
            self.logger.info("Matched one or more onlyBans terms!")
        for url in serverSetup.onlyBansUrls:
            if url in content:
                criteriaMatched += 1
                self.logger.info(f"Matched onlyBans url: {url}")
                break
        if '@here' in content or '@everyone' in content:
            criteriaMatched += 1
            if '@here' in content:
                massPings += 1
            if '@everyone' in content:
                massPings += 1
            self.logger.info(f"mention @everyone or @here attempted!")
        if criteriaMatched >= 2 or massPings > 1:
            self.logger.warning(f"Found likely spambot, msg: {content}")
            return True
        return False

    async def checkRoleAndBans(self, message: discord.Message, serverSetup: ServerSetup, msgOnBan):
        userRoleIds = []
        for role in message.author.roles:
            userRoleIds.append(role.id)
        for role in userRoleIds:
            if role in serverSetup.onlyBansImmuneRoles:
                self.logger.info("User is immune to onlybans")
                await message.delete(delay=5)
                return False
        if msgOnBan:
            await message.reply(embed=self.makeInformationalEmbed(serverSetup.onlyBansMsgReply))
        await self.banSpamBot(serverSetup, message)
        return True

    async def listSubscribers(self, message: discord.Message):

        serverSetup = self.serverSettings.getServer(message.guild.id)
        if not serverSetup.enableOnlyBans:
            await message.channel.send(embed=self.makeErrorEmbed("OnlyBans Premium is not activated on this server"))
            return
        self.startSqlEntry()
        subs = self.db.query(OnlyBansSubscribers).filter(OnlyBansSubscribers.serverId == serverSetup.serverId).all()
        msg = f'{"User Name":<34}{"  Created At":^18} {"  Joined At":^18} {"  Banned At":^18} {"User ID":^21}\n'
        for sub in subs: # type: OnlyBansSubscribers
            msg += sub.pStat
        await self.chunkMsgs(msg, message, codeblock=True, chunkedChar="\n")



    async def banSpamBot(self, serverSetup: ServerSetup, message: discord.Message):
        self.startSqlEntry()
        obChannel = self.db.query(OnlyBansChannel).filter(OnlyBansChannel.serverId == serverSetup.serverId).first() # type: OnlyBansChannel
        obChannel.subscriberCount += 1
        subscriber = OnlyBansSubscribers()
        subscriber.serverId = serverSetup.serverId
        subscriber.userId = message.author.id
        subscriber.userName = message.author.display_name
        subscriber.createdAt = message.author.created_at
        subscriber.joinedAt = message.author.joined_at
        subscriber.bannedAt = datetime.datetime.utcnow()
        self.db.add(obChannel)
        self.db.add(subscriber)

        try:
            await message.guild.ban(message.author, reason="likely Spambot detected", delete_message_days=1)
        except:
            self.logger.error("Unable to ban spambot!")
            trace = traceback.format_exc()
            for line in trace.split('\n'):
                self.logger.error(line)
            self.db.rollback()
            role = message.guild.get_role(serverSetup.onlyBansImmuneRoles[0])
            await message.channel.send(f"{role.mention} unable to ban spam bot, pls help!")
            return

        guild = message.guild
        self.db.commit()
        await self._updateObMessage(guild, serverSetup, obChannel)




    async def _makeObMessage(self, guild: discord.Guild, server: ServerSetup, obChannel: OnlyBansChannel):
        channel = guild.get_channel(obChannel.channelId)
        message = await channel.send(server.onlyBansMessageText.format(obChannel.subscriberCount))
        obChannel.messageId = message.id

    async def _updateObMessage(self, guild: discord.Guild, server: ServerSetup, obChannel: OnlyBansChannel):
        channel = guild.get_channel(obChannel.channelId)
        try:
            message = await channel.fetch_message(obChannel.messageId)
            await message.edit(content=server.onlyBansMessageText.format(obChannel.subscriberCount))
        except:
            self.logger.critical("OB Message update failure!")
            trace = traceback.format_exc()
            for line in trace.split('\n'):
                self.logger.error(line)

    async def initOnlyBans(self):
        self.startSqlEntry()
        for server in self.serverSettings.servers:
            if server.enableOnlyBans:
                obChannel = self.db.query(OnlyBansChannel).filter(OnlyBansChannel.serverId == server.serverId).first()
                if not obChannel:
                    obChannel = OnlyBansChannel()
                    obChannel.serverId = server.serverId
                    obChannel.channelId = 0
                    obChannel.subscriberCount = 0
                    self.db.add(obChannel)
                if server.onlyBansChannel != obChannel.channelId:
                    obChannel.channelId = server.onlyBansChannel
                    await self._makeObMessage(self.rbBot.get_guild(server.serverId), server, obChannel)
                else:
                    guild = self.rbBot.get_guild(server.serverId)
                    channel = guild.get_channel(obChannel.channelId)
                    try:
                        message = await channel.fetch_message(obChannel.messageId)
                    except:
                        await self._makeObMessage(guild, server, obChannel)
        self.db.commit()