try:
    import discord
except:
    pass


class Command:

    def __init__(self, function, helpTxt, module=None, restriction=None, ccBot=None, channels=None):

        self.fncPointer = function
        self.restriction = restriction
        self.rbBot = ccBot
        self.helpTxt = helpTxt
        self.channels = channels
        self.module = module

    async def canRunRestrictedCommand(self, message: discord.Message, command, serverSetup, bMsg=True):
        author = message.author  # type: discord.abc.User
        if self.channels:
            if message.channel.id not in self.channels:
                if bMsg:
                    await message.channel.send("Command not valid in this channel, try another channel")
                return False
        if isinstance(message.channel, discord.DMChannel) and self.rbBot is not None:
            member = await self.rbBot.fetchUser(author.id)
            if member:
                author = member
        try:
            if command.canRun(author, serverSetup):
                return True
            else:
                if bMsg:
                    await message.channel.send("I'm Sorry, I can't let you do that Dave")
        except:
            if bMsg:
                await message.channel.send("Command not available")
        return False

    async def canAccessHelp(self, message, serverSetup):
        if self.helpTxt == '':
            return False
        if self.restriction:
            return await self.canRunRestrictedCommand(message, self.restriction, serverSetup, bMsg=False)
        return True

    async def runCommand(self, message: discord.Message, serverSetup):
        if self.module:
            if not self.module.featureCheck(message.guild.id):
                return
        canRun = True
        if self.restriction:
            canRun = await self.canRunRestrictedCommand(message, self.restriction, serverSetup)
        print('Running Command!')
        if canRun:
            try:
                await message.channel.trigger_typing()
            except:
                pass
            await self.fncPointer(message)
