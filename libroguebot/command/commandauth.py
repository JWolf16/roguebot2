from ..serversettings import ServerSetup

class CommandAuth:

    def __init__(self, owners):
        self.owners = owners
        self.restrict = False
        self.restrictMember = None
        self.restrictedRoles = []
        self.restrictedAuthors = []

    def canRun(self, author, serverSetup: ServerSetup) -> bool:
        if self.restrict:
            try:
                if author.id in self.owners:
                    return True
                for role in author.roles:
                    if role.id in self.restrictedRoles or author.id in self.owners:
                        return True
                if author.id in self.restrictedAuthors:
                    return True
                if self.restrictMember:
                    member = getattr(serverSetup, self.restrictMember)
                    for role in author.roles:
                        if role.id in member:
                            return True
            except:
                pass
            return False
        return True