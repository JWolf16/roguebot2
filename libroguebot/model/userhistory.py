from .base import ModelBase
from sqlalchemy import Column, String, Integer, Boolean, Text, BigInteger, ForeignKey, Float, UnicodeText, Unicode
from sqlalchemy.orm import relationship, backref
from ..data import EBanFlags, BitField32

from ..dataexport import UserHistoryDef, HistoryEntryDef


class User(ModelBase):
    """

    """
    __tablename__ = "user"
    id = Column(Integer, index=True, primary_key=True)
    userId = Column(BigInteger, default=0)
    mentions = Column(Unicode(255), default="")
    userName = Column(Unicode(255), default="")
    notes = Column(UnicodeText, default="")
    banFlags = Column(Integer, default=0)
    history = relationship("HistoryEntry", backref=backref("user", uselist=False))
    ServerGroup = Column(Integer, default=0)

    def toDef(self) -> UserHistoryDef:
        recordDef = UserHistoryDef()
        recordDef.userId = self.userId
        recordDef.mentions = self.mentions
        recordDef.userName = self.userName
        recordDef.notes = self.notes
        recordDef.banFlags = self.banFlags
        recordDef.ServerGroup = self.ServerGroup

        for record in self.history:
            historyDef = record.toDef()
            recordDef.history.append(historyDef)

        return recordDef

    def fromDef(self, recordDef: UserHistoryDef):
        self.banFlags = recordDef.banFlags
        self.userId = recordDef.userId
        self.mentions = recordDef.mentions
        self.userName = recordDef.userName
        self.notes = recordDef.notes
        self.ServerGroup = recordDef.ServerGroup

        for historyDef in recordDef.history:
            entry = HistoryEntry()
            entry.fromDef(historyDef)
            self.history.append(entry)

    def _isFlageSet(self, flag):
        if self.banFlags is None:
            return False
        btField = BitField32(self.banFlags)
        return btField.isSet(flag)

    def _areFlagsSet(self, flags, trueIfAny):
        if self.banFlags is None:
            return False
        btField = BitField32(self.banFlags)
        anySet = False
        allSet = True
        for flag in flags:
            if btField.isSet(flag):
                anySet = True
            else:
                allSet = False
        if trueIfAny:
            return anySet
        return allSet

    def _setFlag(self, flag, bValue):
        if self.banFlags is None:
            self.banFlags = 0
        btField = BitField32(self.banFlags)
        if bValue:
            btField.setBit(flag)
        else:
            btField.unSetBit(flag)
        self.banFlags = btField.Value

    def setFlag(self, flag, bValue=True):
        return self._setFlag(flag, bValue)


    @property
    def isBanned(self):
        return self._isFlageSet(EBanFlags.Banned)

    @property
    def isWarned(self):
        return self._isFlageSet(EBanFlags.Warned)

    @property
    def isMuted(self):
        return self._isFlageSet(EBanFlags.Mute)

    @property
    def isSMuted(self):
        return self._isFlageSet(EBanFlags.SMute)

    @property
    def isAppeal(self):
        return False

    @property
    def isSMutedDropped(self):
        return False

    @property
    def isMutedDropped(self):
        return False

    @property
    def isNote(self):
        return False


class HistoryEntry(ModelBase):
    """

    """
    __tablename__ = "historyentry"
    id = Column(Integer, index=True, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    banFlags = Column(Integer, default=0)
    comments = Column(UnicodeText, default="")
    timeStamp = Column(BigInteger, default=0)
    reporter = Column(Unicode(255), default="")

    def toDef(self) -> HistoryEntryDef:
        recordDef = HistoryEntryDef()
        recordDef.banFlags = self.banFlags
        recordDef.comments = self.comments
        recordDef.timeStamp = self.timeStamp
        recordDef.reporter = self.reporter
        return recordDef

    def fromDef(self, recordDef: HistoryEntryDef):
        self.banFlags = recordDef.banFlags
        self.comments = recordDef.comments
        self.timeStamp = recordDef.timeStamp
        self.reporter = recordDef.reporter

    def _isFlageSet(self, flag):
        if self.banFlags is None:
            return False
        btField = BitField32(self.banFlags)
        return btField.isSet(flag)

    def _areFlagsSet(self, flags, trueIfAny):
        if self.banFlags is None:
            return False
        btField = BitField32(self.banFlags)
        anySet = False
        allSet = True
        for flag in flags:
            if btField.isSet(flag):
                anySet = True
            else:
                allSet = False
        if trueIfAny:
            return anySet
        return allSet

    def _setFlag(self, flag, bValue):
        if self.banFlags is None:
            self.banFlags = 0
        btField = BitField32(self.banFlags)
        if bValue:
            btField.setBit(flag)
        else:
            btField.unSetBit(flag)
        self.banFlags = btField.Value

    def setFlag(self, flag, bValue=True):
        return self._setFlag(flag, bValue)

    @property
    def isBanned(self):
        return self._isFlageSet(EBanFlags.Banned)

    @property
    def isWarned(self):
        return self._isFlageSet(EBanFlags.Warned)

    @property
    def isMuted(self):
        return self._isFlageSet(EBanFlags.Mute)

    @property
    def isSMuted(self):
        return self._isFlageSet(EBanFlags.SMute)

    @property
    def isAppeal(self):
        return self._isFlageSet(EBanFlags.Appealed)

    @property
    def isSMutedDropped(self):
        return self._isFlageSet(EBanFlags.SMuteDropped)

    @property
    def isMutedDropped(self):
        return self._isFlageSet(EBanFlags.MuteDropped)

    @property
    def isNote(self):
        return self._isFlageSet(EBanFlags.ModNote)