from .base import ModelBase
from ..data import ETimeType
from sqlalchemy import Column, String, Integer, Boolean, Text, BigInteger, ForeignKey, Float, JSON, DateTime
from sqlalchemy.orm import relationship, backref
from datetime import datetime
import time
from datetime import timedelta, datetime

from ..dataexport import DiscordTicketDef


class DiscordTicket(ModelBase):
    """

    """
    __tablename__ = "discordTicket"
    id = Column(Integer, index=True, primary_key=True)
    userId = Column(BigInteger, default=0)
    ticketId = Column(BigInteger, default=0)
    channelId = Column(BigInteger, default=0, index=True)
    guildId = Column(BigInteger, default=0)
    alerted = Column(Boolean, default=False)

    def toDef(self) -> DiscordTicketDef:
        recordDef = DiscordTicketDef()
        recordDef.guildId = self.guildId
        recordDef.ticketId = self.ticketId
        recordDef.userId = self.userId
        recordDef.channelId = self.channelId
        recordDef.alerted = self.alerted
        return recordDef

    def fromDef(self, recordDef: DiscordTicketDef):
        self.guildId = recordDef.guildId
        self.ticketId = recordDef.ticketId
        self.userId = recordDef.userId
        self.channelId = recordDef.channelId
        self.alerted = recordDef.alerted