from .base import ModelBase
from ..data import ETimeType
from sqlalchemy import Column, String, Integer, Boolean, Text, BigInteger, ForeignKey, Float, Unicode, UnicodeText
from sqlalchemy.orm import relationship, backref
import time
from datetime import timedelta, datetime

from ..dataexport import ReminderDef


class Reminders(ModelBase):
    """

    """
    __tablename__ = "reminders"
    id = Column(Integer, index=True, primary_key=True)
    userId = Column(BigInteger, default=0)
    mentions = Column(Unicode(255), default="")
    remindTime = Column(BigInteger, default=0)
    channelId = Column(BigInteger, default=0)
    guildId = Column(BigInteger, default=0)
    reminderText = Column(UnicodeText, default="")

    def toDef(self) -> ReminderDef:
        recordDef = ReminderDef()
        recordDef.guildId = self.guildId
        recordDef.mentions = self.mentions
        recordDef.userId = self.userId
        recordDef.channelId = self.channelId
        recordDef.remindTime = self.remindTime
        recordDef.reminderText = self.reminderText
        return recordDef

    def fromDef(self, recordDef: ReminderDef):
        self.guildId = recordDef.guildId
        self.mentions = recordDef.mentions
        self.userId = recordDef.userId
        self.channelId = recordDef.channelId
        self.remindTime = recordDef.remindTime
        self.reminderText = recordDef.reminderText

    def setTime(self, amount, tType):
        cTime = time.time()
        delta = None
        if tType == ETimeType.Weeks:
            delta = timedelta(weeks=amount)
        elif tType == ETimeType.Days:
            delta = timedelta(days=amount)
        elif tType == ETimeType.Hours:
            delta = timedelta(hours=amount)
        else:
            delta = timedelta(minutes=amount)
        self.remindTime = cTime + delta.total_seconds()

    @property
    def reminderTime(self):
        return datetime.fromtimestamp(self.remindTime).strftime('%b %d, %Y - %H:%M:%S, %Z')

    @property
    def text(self):
        if self.reminderText is None:
            return ""
        return self.reminderText
