from .base import ModelBase
from sqlalchemy import Column, String, Integer, Boolean, Text, BigInteger, ForeignKey, Float, Unicode, UnicodeText
from sqlalchemy.orm import relationship, backref
from ..data import EBanFlags, BitField32

from ..dataexport import ArchiveRecordDef


class ArchiveRecord(ModelBase):
    """

    """
    __tablename__ = "archiverecord"
    id = Column(Integer, index=True, primary_key=True)
    guildId = Column(BigInteger, default=0)
    fileName = Column(Unicode(255), default="")
    channelName = Column(Unicode(255), default="")
    channelId = Column(BigInteger, default=0)
    timestamp = Column(BigInteger, default=0)

    def toDef(self) -> ArchiveRecordDef:
        recordDef = ArchiveRecordDef()
        recordDef.guildId = self.guildId
        recordDef.fileName = self.fileName
        recordDef.channelName = self.channelName
        recordDef.channelId = self.channelId
        recordDef.timestamp = self.timestamp
        return recordDef

    def fromDef(self, recordDef: ArchiveRecordDef):
        self.guildId = recordDef.guildId
        self.fileName = recordDef.fileName
        self.channelName = recordDef.channelName
        self.channelId = recordDef.channelId
        self.timestamp = recordDef.timestamp
