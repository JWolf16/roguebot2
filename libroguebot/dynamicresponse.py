from .data import BaseDataObject, SubObjectMap
from .eresponsetype import EResponseType
import random

class PossibleResponse(BaseDataObject):

    XML_Type = "PossibleResponse"

    def __init__(self):
        self.Response = ""
        self.ResponseType = EResponseType.NoResponse

class DynamicResponse(BaseDataObject):

    XML_Type = "DynamicResponse"

    _SubObjectMapping = [
        SubObjectMap(PossibleResponse, '_possibleResponses')
    ]

    def __init__(self):
        self.command = ""
        self.Response = ""
        self.ResponseType = EResponseType.NoResponse
        self.pingRoles = []
        self.helpText = ""
        self.showInHelp = False
        self.showInMemes = False
        self.restrict = False
        self.restrictFromOwners = False
        self.asEmbed = False
        self.embedColour = 0x71368a
        self.restrictedRoles = []
        self.restrictedUsers = []
        self._possibleResponses = [] #type: list[PossibleResponse]

    @property
    def possibleResponses(self):
        return self._possibleResponses

    def getResponse(self) -> PossibleResponse:
        return random.choice(self._possibleResponses)

    def canRun(self, author, owners):
        if self.restrict:
            try:
                for role in author.roles:
                    if role.id in self.restrictedRoles:
                        return True
                if author.id in owners and not self.restrictFromOwners:
                    return True
                if author.id in self.restrictedUsers:
                    return True
            except:
                pass
            return False
        return True

    def getExtras(self, message):
        extras = ""
        ltItems = message.split(" ")
        if len(ltItems) > 1:
            extras = ltItems[1]
        return extras
