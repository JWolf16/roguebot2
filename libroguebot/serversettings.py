from .data import SubObjectMap, BaseDataObject, EServerGroup
from .reactforroles import ReactForRole


class SubTicketChannels(BaseDataObject):

    XML_Type = 'SubChannelTickets'

    def __init__(self):
        self.cmdType = ''


class ServerSetup(BaseDataObject):

    XML_Type = 'ServerSetup'

    _SubObjectMapping = [
        SubObjectMap(ReactForRole, '_reactRoles', 'reactRoles')
    ]

    def __init__(self):
        self.isActive = True
        self.serverId = 0
        self.serverName = ''
        self.ticketMsg = ''
        self.ticketMsgEmbed = False
        self.ticketMsgEmbedColour = 0x57F287
        self.ticketMods = []
        self.ticketRoles = []
        self.ticketCat = ''
        self.dmOnTicketClose = False
        self.dmTicketMsgPrefix = 'Your ticket {0} on {1} has been actioned on'
        self.dmTicketMsg = 'The issue has been addressed, is waiting on a specific developer or requires longer term investigation and further input is not required at this time'
        self.closedTicketCat = ''
        self.pingOnTicketsFull = []
        self.pingOnTicketRoles = []
        self.raffleCmdRoles = []
        self.raffleEntryRoles = []
        self.ticketPrefix = 'ThereIsNoTicketHere'
        self.ticketDeleteDelay = 0
        self.moderatorCmdRoles = []
        self.muteRole = 0
        self.supportMuteRole = 0
        self.muteCmdRoles = []
        self.allowPinkMechsText = False
        self.reporterGroup = EServerGroup.Unknown
        self.enhancedWelcomeLog = False
        self.welcomeLogStrFmt = '%b %d, %Y - %H:%M:%S, %Z'
        self.welcomeLogChannel = 0
        self.archiveStrFmt = '%b %d, %Y'
        self.reminderMsg = 'Reminder Poke!'
        self.reminderMeme = None
        self.enableMsgDeleteRecords = False
        self.messageDeleteChannel = 0
        self.messageDeleteExempt = []
        self.messageDeleteCategoryExempt = []
        self.enableOnlyBans = False
        self.onlyBansChannel = 0
        self.onlyBansMessageText = ""
        self.onlyBansImmuneRoles = []
        self.onlyBansTerms = []
        self.onlyBansUrls = []
        self.onlyBansMsgReply = ""

        self.reactOnRoleChannel = 0
        self._reactRoles = [] # type: list[ReactForRole]

    def getRolesOnApply(self, emoji):
        if self.reactOnRoleChannel == 0:
            return
        for react in self._reactRoles:
            if react.emoji == emoji:
                return react.roleIds
        return


class ServerSettings(BaseDataObject):

    XML_Type = 'ServerSetting'

    _SubObjectMapping = [
        SubObjectMap(ServerSetup, '_servers', 'Servers')
    ]

    def __init__(self):
        self._servers = []  # type: list[ServerSetup]
        self._onlyBansChannels = []
        self.serverMap = {}

    def getServer(self, serverId) -> ServerSetup:
        for server in self._servers:
            if server.serverId == serverId and server.isActive:
                return server
        return ServerSetup()



    def updateCaches(self):
        self._onlyBansChannels.clear()
        for server in self._servers:
            if server.enableOnlyBans and server.onlyBansChannel != 0:
                self._onlyBansChannels.append(server.onlyBansChannel)

    @property
    def servers(self):
        return self._servers

    @property
    def onlyBansChannels(self):
        return self._onlyBansChannels
