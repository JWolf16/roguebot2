

class EResponseType:

    NoResponse = 0
    String = 1
    File = 2
    Link = 3
    UseMessageExtras = 4
    RandomChoose = 5
    Alias = 6
    RestrictedAlias = 7
    ChannelReHome = 8
